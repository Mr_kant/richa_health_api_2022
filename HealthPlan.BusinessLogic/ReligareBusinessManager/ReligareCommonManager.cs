﻿using HealthPlan.BusinessLogic.CacheBussinessManager;
using HealthPlan.Data.Models.GeneralProposal;
using HealthPlan.Data.Models.Referance;
using HealthPlan.Data.Models.Religare;
using HealthPlan.DataAccessLayer.ReligareDataManager;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HealthPlan.BusinessLogic.ReligareBusinessManager
{
   public class ReligareCommonManager
    {
       
        public static IEnumerable<Question> GetQuestion(int? productId) 
        {          
            return ReligareCommonDataManager.GetQuestion(productId);
        }
 

        public static IEnumerable<Api> GetApiPolicyPDFDetails(int ProductDetailId, string UrlType)
        {
            return ReligareCache.GetApiPolicyPDFDetails(ProductDetailId, UrlType);
        }


        public static bool  ClearCache()
        {
            return ReligareCache.ResetCache();
        }

        
    }
}

﻿using HealthPlan.Common.Global;
using HealthPlan.Common.RestApis;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.GeneralProposal;
using HealthPlan.Data.Models.GroupCare;
using HealthPlan.Data.Models.Referance;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static HealthPlan.Data.Models.Religare.ReligareMasterData;

namespace HealthPlan.BusinessLogic.GroupCareBusinessManager
{
    public class GroupCareProposal
    {
        public static string Error { get; private set; }
        public static async Task<ProposalResponse> GroupCareProposalInd(GroupQuotationInd groupCare, List<ApiRequestResponseLog> ApiRequestResponseLog, string Inquiry_id)
        {
            string url;
            Error = "";

            IEnumerable<Api> api = GroupCareCommonManager.GetApiDetails(1, "createPolicy");
            RestApi<GroupQuotationInd> rest = new RestApi<GroupQuotationInd>();
            rest.baseaddress = api.FirstOrDefault().BaseAddress;
            url = api.FirstOrDefault().Url;

            var requestheader = new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string, string>("AppID", AppSettings.ReligareAppId),
                            new KeyValuePair<string, string>("Signature", AppSettings.ReligareSignature),
                            new KeyValuePair<string, string>("TimeStamp", AppSettings.ReligareTimeStamp)
                    };
            var requestStr = JsonConvert.SerializeObject(groupCare);
            logger.CreateLogFile(JsonConvert.SerializeObject(groupCare), "Proposal_Request");

            List<ApiRequestResponseLog> objStore = new List<ApiRequestResponseLog>();
            ApiRequestResponseLog obstore = new ApiRequestResponseLog() {Inquiry_id= Inquiry_id,CompanyName= "Care Health", ApiMethod= "ReqStore",Request= requestStr };
            objStore.Add(obstore);
            SearchBusinessManager.ProductDetailBAL.ApiLog(objStore);

            var value = await rest.PostApi(url, groupCare, requestheader);
            logger.CreateLogFile(JsonConvert.SerializeObject(value), "Proposal_Response");

            PurposalErrorResponse data = null;
            ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = Inquiry_id, ApiMethod = "Proposal", CompanyName = "Group Care", Request = requestStr, Response = value.ToString() });

            try
            {
                if (rest.status == false)
                {
                    Error = value;
                    return null;
                }
                data = JsonConvert.DeserializeObject<PurposalErrorResponse>(value);
                if (data.intPolicyDataIO.errorLists.Count() == 0 && data.responseData.status == "1")
                {
                    ProposalResponse response = new ProposalResponse()
                    {
                        CompanyId = data.intPolicyDataIO.policy.CompanyId,
                        totalPremium = data.intPolicyDataIO.policy.premium.ToString(),

                        ProposalNum = data.intPolicyDataIO.policy.proposalNum
                    };
                    return response;
                }
                Error = JsonConvert.SerializeObject(data.intPolicyDataIO.errorLists);
                return null;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public static async Task<ProposalResponse> GroupCarePerposalFloat(GroupQuotationInd groupCare, List<ApiRequestResponseLog> ApiRequestResponseLog, string Inquiry_id)
        {
            string url;
            Error = "";


            IEnumerable<Api> api = GroupCareCommonManager.GetApiDetails(1, "createPolicy");
            //RestFullApi<ReligareMasterData> rest = new RestFullApi<ReligareMasterData>();
            RestApi<GroupQuotationInd> rest = new RestApi<GroupQuotationInd>();


            rest.baseaddress = api.FirstOrDefault().BaseAddress;
            url = api.FirstOrDefault().Url;

            var requestheader = new List<KeyValuePair<string, string>>() {
                            new KeyValuePair<string, string>("AppID", AppSettings.ReligareAppId),
                            new KeyValuePair<string, string>("Signature", AppSettings.ReligareSignature),
                            new KeyValuePair<string, string>("TimeStamp", AppSettings.ReligareTimeStamp)
                    };
            var requestStr = JsonConvert.SerializeObject(groupCare);
            var value = await rest.PostApi(url, groupCare, requestheader);
            PurposalErrorResponse data = null;
            ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = Inquiry_id, ApiMethod = "Proposal", CompanyName = "Group Care", Request = requestStr, Response = value.ToString() });

            try
            {
                if (rest.status == false)
                {
                    Error = value;
                    return null;
                }
                data = JsonConvert.DeserializeObject<PurposalErrorResponse>(value);
                if (data.intPolicyDataIO.errorLists.Count() == 0 && data.responseData.status == "1")
                {
                    ProposalResponse response = new ProposalResponse()
                    {
                        CompanyId = data.intPolicyDataIO.policy.CompanyId,
                        //referenceId = "N/A",
                        totalPremium = data.intPolicyDataIO.policy.premium.ToString(),
                        //serviceTax = "N/A",
                        //totalPremium = "N/A",
                        ProposalNum = data.intPolicyDataIO.policy.proposalNum
                    };
                    return response;
                }
                Error = JsonConvert.SerializeObject(data.intPolicyDataIO.errorLists);
                return null;

            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }



        }
        public static string GetError()
        {
            var error = Error;
            Error = string.Empty;
            return error;
        }
    }
}


﻿
using HealthPlan.Data.Models.Referance;
using System;
using System.Collections.Generic;
using System.Text;
using HealthPlan.Data.Models.GroupCare;

namespace HealthPlan.BusinessLogic.GroupCareBusinessManager
{
    public class GroupCareCommonManager
    {
        public static GroupCareProducts GetGroupCareProduct(int ProductDetailId)
        {
            return GroupCareCache.GetCareProductInfo(ProductDetailId);
        }

      
        public static IEnumerable<Api> GetApiDetails(int CompanyID, string UrlType)
        {
            return GroupCareCache.GetApiInfo(CompanyID, UrlType);
        }
        public static IEnumerable<GroupCarePartyRelation> GetRelationShip()
        {
            return GroupCareCache.GetRelationShip();
        }

        public static IEnumerable<GroupCareSuminsured> GetSuminsureds(int Suminsured, int CoverId)
        {
            return GroupCareCommonDataManager.GetSuminsureds(Suminsured, CoverId);
        }

        public static IEnumerable<GroupCareAddons> GetAddon()
        {
            return GroupCareCommonDataManager.GetAddons();
        }
    }
}

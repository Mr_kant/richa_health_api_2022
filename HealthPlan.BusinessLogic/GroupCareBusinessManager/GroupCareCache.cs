﻿using HealthPlan.Common.Cache;
using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Data.Models.GroupCare;
using HealthPlan.Data.Models.Referance;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.BusinessLogic.GroupCareBusinessManager
{
    class GroupCareCache
    {
        private static string ReligareSumInsured = "Religare-SumInsured";
        private static string ReligareRelationShipInfo = "Religare-RelationShipInfo";
        private static string ReligareQuestion = "Religare-Question";
        private static string ReligareApiDetails = "Religare-ApiDetails";
        private static string ReligarePolicyPDFDetails = "Religare-PolicyPDFDetails";
        private static string ManipalPartyRelation = "Manipal-PartyRelation";

        private static string StarthealthApi = "Starthealth-Api";

        public static IEnumerable<Api> GetApiInfo(int CompanyID, string UrlType)
        {

            var cacheKey = StarthealthApi + "-" + CompanyID + "-" + UrlType;


            var cachedData = ObjectCacheUtil.Get<IEnumerable<Api>>(cacheKey);

            if (cachedData.IsEnumerableNullOrEmpty())
            {
                cachedData = GroupCareCommonDataManager.GetApiInfo(CompanyID, UrlType);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }

            return cachedData;

        }
        public static GroupCareProducts GetCareProductInfo(int ProductDetailId)
        {
            var cacheKey = $"{ReligareSumInsured}-{ProductDetailId}";

            var cachedData = ObjectCacheUtil.Get<GroupCareProducts>(cacheKey);

            if (cachedData == null)
            {
                cachedData = GroupCareCommonDataManager.GetGroupCareProduct(ProductDetailId);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }

            return cachedData;
        }

        public static IEnumerable<Api> GetApiDetails(string baseProductCode, string UrlType)
        {
            var cacheKey = $"{ReligareApiDetails}-{ baseProductCode }-{UrlType}";

            var cachedData = ObjectCacheUtil.Get<IEnumerable<Api>>(cacheKey);

            if (cachedData.IsEnumerableNullOrEmpty())
            {
                cachedData = GroupCareCommonDataManager.GetApiInfo(baseProductCode, UrlType);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }
            return cachedData;
        }
        //public static IEnumerable<PartyRelation> GetRelationShip()
        //{
        //    var cacheKey = ManipalPartyRelation;

        //    var cachedData = ObjectCacheUtil.Get<IEnumerable<PartyRelation>>(cacheKey);

        //    if (cachedData.IsEnumerableNullOrEmpty())
        //    {
        //        cachedData = GroupCareCommonDataManager.GetRelationShipInfo();
        //        ObjectCacheUtil.Set(cacheKey, cachedData);
        //    }
        //    return cachedData;
        //}

        public static IEnumerable<GroupCarePartyRelation> GetRelationShip()
        {
            var cacheKey = ManipalPartyRelation;

            var cachedData = ObjectCacheUtil.Get<IEnumerable<GroupCarePartyRelation>>(cacheKey);

            if (cachedData.IsEnumerableNullOrEmpty())
            {
                cachedData = GroupCareCommonDataManager.GetRelationShipInfo();
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }
            return cachedData;
        }
    }
}
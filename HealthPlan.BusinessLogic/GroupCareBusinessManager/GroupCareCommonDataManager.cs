﻿using Dapper;
using HealthPlan.Data.Models.Authenticate;
//using HealthPlan.Data.Models.Manipal;
using HealthPlan.Data.Models.GroupCare;
using HealthPlan.Data.Models.Referance;
using HealthPlan.DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthPlan.BusinessLogic.GroupCareBusinessManager
{
    class GroupCareCommonDataManager
    {
        public static GroupCareProducts GetGroupCareProduct(int ProductId)
        {
            // ResponseModel response = new ResponseModel();
            GroupCareProducts groupcareProduct = new GroupCareProducts();
            using (var db = Connection.Open())
            {
                using (var multi = db.QueryMultiple("sp_Get_SuminsuredCode_Religare", new
                {
                    ProductId
                }, commandType: System.Data.CommandType.StoredProcedure))
                {
                    groupcareProduct.GroupCareProduct = multi.Read<GroupCareProduct>().ToList();
                    var BaseProductCode = multi.Read<string>();
                    groupcareProduct.BaseProductCode = BaseProductCode.FirstOrDefault();
                }
            }
            return groupcareProduct;

        }

        internal static IEnumerable<GroupCareSuminsured> GetSuminsureds(int Suminsured, int CoverId)
        {
            IEnumerable<GroupCareSuminsured> SuminsuredDetails = new List<GroupCareSuminsured>();
            using (var db = Connection.Open())
            {

                const string command = @"select * from tbl_SumInsured_Groupcare where Suminsured=@Suminsured and CoverageId =@CoverId";
                SuminsuredDetails  = db.Query<GroupCareSuminsured>(command, new
                {
                    Suminsured,
                    CoverId
                });

            }

            return SuminsuredDetails;
        }

        public static IEnumerable<Api> GetApiInfo(int CompanyID, string UrlType)
        {

            IEnumerable<Api> api = new List<Api>();
            using (var db = Connection.Open())
            {

                const string command = @"select*from tbl_Api_GroupCare360 where CompanyID=@CompanyID and UrlType =@UrlType";
                api = db.Query<Api>(command, new
                {
                    CompanyID,
                    UrlType
                });

            }

            return api;

        }
        public static IEnumerable<Api> GetApiInfo(string CompanyId, string UrlType)
        {

            IEnumerable<Api> api = new List<Api>();
            using (var db = Connection.Open())
            {
                const string command = @"select * from tbl_Api_GroupCare360  where CompanyId = @CompanyId and UrlType =@UrlType";
                api = db.Query<Api>(command, new
                {
                    CompanyId,
                    UrlType,
               
                });
            }

            return api;

        }

        //public static IEnumerable<PartyRelation> GetRelationShipInfo()
        //{
        //    ResponseModel response = new ResponseModel();
        //    IEnumerable<PartyRelation> relations = new List<PartyRelation>();
        //    using (var db = Connection.Open())
        //    {

        //        const string command = @"select * from T_Relationship_Common";
        //        relations = db.Query<PartyRelation>(command, new { });

        //    }

        //    return relations;

        //}

        public static IEnumerable<GroupCarePartyRelation> GetRelationShipInfo()
        {
            IEnumerable<GroupCarePartyRelation> relations = new List<GroupCarePartyRelation>();
            using (var db = Connection.Open())
            {

                const string command = @"select * from tbl_GroupCare_RelationShip";
                relations = db.Query<GroupCarePartyRelation>(command, new { });
                 
            }

            return relations;

        }

        public static IEnumerable<GroupCareAddons> GetAddons()
        {
            IEnumerable<GroupCareAddons> Addons = new List<GroupCareAddons>();
            using (var db = Connection.Open())
            {

                const string command = @"select * from tbl_Care_AddOns_GroupCare360";
                Addons = db.Query<GroupCareAddons>(command, new { });

            }

            return Addons;
        }
    }
}

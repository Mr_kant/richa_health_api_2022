﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.BusinessLogic.GroupCareBusinessManager
{
    interface IGroupCareProduct
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public bool Status { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int CompanyId { get; set; }
        public string ProductCode { get; set; }
        public string Product_Description { get; set; }
        public int GSTPercenatge { get; set; }
        public string tableName { get; set; }
        public string Period { get; set; }
        public string PremiumtableName { get; set; }
    }
}

﻿using HealthPlan.BusinessLogic.GroupCareBusinessManager;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.GroupCare;
using HealthPlan.Data.Models.Product;
using HealthPlan.Data.Models.SearchProduct;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace HealthPlan.BusinessLogic.SearchBusinessManager
{
    public class SearchProductIndivisual
    {
        public static List<GroupCareQuotesResponse> GetQuotesCareHealth(int mainage, Productfilter filterdata, List<KeyValuePair<string, int?>> menbers, ProductSearchModel clientData, List<ApiRequestResponseLog> ApiRequestResponseLog)
        {
            IEnumerable<GroupCareAddons> AddonsList= GroupCareCommonManager.GetAddon();
            
            DateTime currentDate = DateTime.Now;
            DateTime Dob = currentDate.AddYears(-mainage);
            List<GroupCareQuotesResponse> GroupCarepostResponses = new List<GroupCareQuotesResponse>();
            var insured = new List<dynamic>();
            foreach (KeyValuePair<string, int?> insure in menbers)
            {
                insured.Add(new { dob = insure.Value, buyBackPED = 0 });
            }

            foreach (var item in clientData.productGroupCare)
            {
                if (item.Tenure == 1 && item.period != "NA") // we only calculate for tenure 2, 3 if tenure is 1 
                {
                    var periods = item.period.Split(",");
                    foreach (var perd in periods)
                    {
                        GroupCareQuotesResponse data = new GroupCareQuotesResponse();
                        List<AddOnses> add = new List<AddOnses>();
                        foreach (var items in AddonsList)
                        {
                            add.Add(new AddOnses
                            {
                                AddOns = items.AddOns,
                                Value = items.Value,
                                Description = items.Description
                            });
                           
                        }
                        data.Addons = add;
                        data.CompanyName = item.CompanyName;
                        data.CompanyId = item.CompanyId;
                        data.logoUrl = item.logoUrl;
                        data.ProductName = item.ProductName;
                        data.GSTPercenatge = item.GSTPercenatge;
                        data.period = item.period;
                        data.ProductCode = item.ProductCode;
                        data.Tenure = item.Tenure;
                        data.SumInsuredCode = item.SumInsuredCode;
                        data.SchemaId = item.SchemaId;
                        data.SchemeId = item.SchemeId;
                        data.discountPercent = item.discountPercent;
                        data.ProductDetailID = item.ProductDetailID;
                        data.SumInsuredValue = Convert.ToInt32(item.SumInsuredValue);
                        data.Policytype = filterdata.policytype;
                        //int schemaID = 0;
                        //int.TryParse(item.SchemeId, out schemaID);
                        data.SchemaId = item.SchemaId; //int.Parse(item.SchemaId);
                        data.SuminsuredId = item.SuminsuredId;
                        data.Plan = "";
                        data.AgeBracket = item.AgeBracket;
                        decimal? premium = Convert.ToDecimal(item.premium);


                        if (data.discountPercent == 0)
                        {
                            data.discountPercent = 0.00m;
                        }

                        data.discountPercent = Math.Round(data.discountPercent, 2);
                        int period = 1;
                        int.TryParse(perd, out period);
                        data.BasePremium = string.Format("{0:0.00}", (premium * period)).ToString();
                        data.premium = string.Format("{0:0.00}", (premium * period)).ToString();
                        data.serviceTax = string.Format("{0:0.00}", ProductDetailBAL.calculateTax(Convert.ToInt32(perd), premium, item.GSTPercenatge)).ToString();
                        data.totalPremium = string.Format("{0:0.00}", ProductDetailBAL.calculatePermium(Convert.ToInt32(perd), premium, item.GSTPercenatge)).ToString();

                        data.product = new
                        {
                            policyTypeName = item.ProductName,
                            policyName = item.ProductName,
                            period = perd,
                            //schemeId = schemaID,
                            insureds = insured,
                            productDetailId = item.ProductDetailID
                        };

                        data.Inquiry_id = filterdata.Inquiry_id;
                        GroupCarepostResponses.Add(data);
                        var requestStr = JsonConvert.SerializeObject(item);
                        var responseStr = JsonConvert.SerializeObject(data);
                        ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = filterdata.Inquiry_id, ApiMethod = "QuickQuotes", CompanyName = "Care Health", Request = requestStr, Response = responseStr });

                    }
                }
                else
                {
                    GroupCareQuotesResponse data = new GroupCareQuotesResponse();
                    List<AddOnses> add = new List<AddOnses>();
                    foreach (var items in AddonsList)
                    {
                        add.Add(new AddOnses
                        {
                            AddOns = items.AddOns,
                            Value = items.Value
                        });
                       
                    }
					 data.Addons = add;
                    //    data.Id = item.id;
                    data.CompanyName = item.CompanyName;
                    data.CompanyId = item.CompanyId;
                    data.logoUrl = item.logoUrl;
                    data.SumInsuredValue = Convert.ToInt32(item.SumInsuredValue);
                    data.Policytype = filterdata.policytype;
                    int schemaID = 0;
                    // int.TryParse(item.SchemaId, out schemaID);
                    data.SchemaId = item.SchemaId;
                    data.SchemaId = schemaID;
                    data.SumInsuredValue = item.SuminsuredId;
                    data.Plan = "";
                    data.AgeBracket = item.AgeBracket;
                    decimal? premium = Convert.ToDecimal(item.premium);

                    if (data.discountPercent == 0)
                    {
                        data.discountPercent = Convert.ToInt32(0.00m);
                    }

                    data.discountPercent = Math.Round(data.discountPercent, 2);

                    data.BasePremium = string.Format("{0:0.00}", item.premium).ToString();
                    data.premium = string.Format("{0:0.00}", premium).ToString();
                    data.serviceTax = string.Format("{0:0.00}", ProductDetailBAL.calculateTax(Convert.ToInt32(1), premium, item.GSTPercenatge)).ToString();
                    data.totalPremium = string.Format("{0:0.00}", ProductDetailBAL.calculatePermium(Convert.ToInt32(1), premium, item.GSTPercenatge)).ToString();

                    data.product = new
                    {
                        policyTypeName = item.ProductName,
                        policyName = item.ProductName,
                        period = item.Tenure,
                        //schemeId = schemaID,
                        insureds = insured,
                        productDetailId = item.ProductDetailID,
                    };

                    data.Inquiry_id = filterdata.Inquiry_id;
                    GroupCarepostResponses.Add(data);
                    var requestStr = JsonConvert.SerializeObject(item);
                    var responseStr = JsonConvert.SerializeObject(data);
                    ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = filterdata.Inquiry_id, ApiMethod = "QuickQuotes", CompanyName = "Care Health", Request = requestStr, Response = responseStr });
                }
            }
            return GroupCarepostResponses;
        }
        //public static async Task<List<StarHealthIndividualQuotesResponse>> QutesRequestAsync(ProductSearchModel clientData, int pincode, Productfilter productfilter, List<ApiRequestResponseLog> ApiRequestResponseLog)
        //{
        //    var tasks = new List<Task<string>>();
        //    DateTime currentDate = DateTime.Now;
        //    string logoUrl = clientData.productStarHealths.FirstOrDefault().logoUrl;
        //    string InsuranceCompany = clientData.productStarHealths.FirstOrDefault().CompanyName;
        //    int InsuranceCompanyId = clientData.productStarHealths.FirstOrDefault().CompanyId;
        //    string PolicyType = productfilter.policytype;
        //    string suminsured = productfilter.Suminsured;
        //    List<QuickQuotesModelIndividuals> quickQuoteslist = new List<QuickQuotesModelIndividuals>();


        //    foreach (var item in clientData.productStarHealths)
        //    {

        //        QuickQuotesModelIndividuals quickQuotes = new QuickQuotesModelIndividuals();
        //        quickQuotes.Id = item.Id;
        //        quickQuotes.APIKEY = AppSettings.StarhealthApikey;
        //        quickQuotes.SECRETKEY = AppSettings.StarhealthApiSecret;
        //        quickQuotes.policyTypeName = item.ProductCode;
        //        quickQuotes.policyName = item.ProductName;
        //        quickQuotes.postalCode = pincode;
        //        quickQuotes.period = Convert.ToInt32(item.Period);
        //        quickQuotes.schemeId = item.SchemeId;
        //        quickQuotes.productDetailId = item.ProductDetailId;
        //        quickQuotes.suminsured = Convert.ToInt32(item.SumInsured);
        //        quickQuotes.Plan = item.Plan;
        //        quickQuotes.AgeBracket = item.AgeBracket;
        //        quickQuotes.suminsuredId = item.SumInsuredId;
        //        quickQuotes.BasePremium = item.BasePremium;
        //        quickQuotes.insureds = new List<BasicIndividiual>();
        //        if (productfilter.SelfAge != null && productfilter.SelfAge > 0)
        //        {
        //            DateTime Dob = currentDate.AddYears((int)-productfilter.SelfAge);
        //            BasicIndividiual insured = new BasicIndividiual();
        //            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //            insured.buyBackPED = 0;
        //            insured.sumInsuredId = item.SumInsuredId;
        //            quickQuotes.insureds.Add(insured);
        //        }
        //        if (productfilter.SpouseAge != null && productfilter.SpouseAge > 0)
        //        {
        //            DateTime Dob = currentDate.AddYears((int)-productfilter.SpouseAge);
        //            BasicIndividiual insured = new BasicIndividiual();
        //            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //            insured.buyBackPED = 0;
        //            insured.sumInsuredId = item.SumInsuredId;

        //            quickQuotes.insureds.Add(insured);
        //        }
        //        if (productfilter.SonAge != null && !(productfilter.SonAge?.Length == 1) && !(productfilter.SonAge?[0] == 0))
        //        {
        //            for (int a = 0; a < productfilter.SonAge?.Length; a++)
        //            {
        //                DateTime Dob = currentDate.AddYears((int)-productfilter.SonAge?[a]);
        //                BasicIndividiual insured = new BasicIndividiual();
        //                insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //                insured.buyBackPED = 0;
        //                insured.sumInsuredId = item.SumInsuredId;
        //                quickQuotes.insureds.Add(insured);

        //            }
        //        }
        //        if (productfilter.DaughterAge != null && !(productfilter.DaughterAge?.Length == 1) && !(productfilter.DaughterAge?[0] == 0))
        //        {
        //            for (int a = 0; a < productfilter.DaughterAge?.Length; a++)
        //            {
        //                DateTime Dob = currentDate.AddYears((int)-productfilter.DaughterAge?[a]);
        //                BasicIndividiual insured = new BasicIndividiual();
        //                insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //                insured.buyBackPED = 0;
        //                insured.sumInsuredId = item.SumInsuredId;
        //                quickQuotes.insureds.Add(insured);

        //            }
        //        }

        //        if (productfilter.FatherAge != null && productfilter.FatherAge > 0)
        //        {
        //            DateTime Dob = currentDate.AddYears((int)-productfilter.FatherAge);
        //            BasicIndividiual insured = new BasicIndividiual();
        //            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //            insured.buyBackPED = 0;
        //            insured.sumInsuredId = item.SumInsuredId;
        //            quickQuotes.insureds.Add(insured);
        //        }
        //        if (productfilter.MotherAge != null && productfilter.MotherAge > 0)
        //        {
        //            DateTime Dob = currentDate.AddYears((int)-productfilter.MotherAge);
        //            BasicIndividiual insured = new BasicIndividiual();
        //            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
        //            insured.buyBackPED = 0;
        //            insured.sumInsuredId = item.SumInsuredId;
        //            quickQuotes.insureds.Add(insured);
        //        }


        //        quickQuoteslist.Add(quickQuotes);
        //        //}
        //    }
        //    logger.Log("Set Api Headers and Rreated Request", nameof(SearchProductIndivisual), 40);
        //    string Url;
        //    RestApi<QuickQuotesModelIndividuals> rest = new RestApi<QuickQuotesModelIndividuals>();
        //    IEnumerable<Api> api = GroupCareCommonManager.GetApiDetails(1, "Quotes");
        //  //  IEnumerable<Api> api = Starhealthcommonmanager.GetApiDetails(1, "Quotes");
        //    rest.baseaddress = api.FirstOrDefault().BaseAddress;
        //    Url = api.FirstOrDefault().Url;

        //    foreach (var post in quickQuoteslist)
        //    {

        //        Task<string> func()
        //        {
        //            return rest.PostApi(Url, post);
        //        }
        //        tasks.Add(func());
        //    }
        //    logger.Log("Wating for response", nameof(SearchProductIndivisual), 58);
        //    await Task.WhenAll(tasks.ToArray());

        //    List<StarHealthIndividualQuotesResponse> postResponses = new List<StarHealthIndividualQuotesResponse>();
        //    logger.Log("Processing response", nameof(SearchProductIndivisual), 60);

        //    var i = 0;
        //    var clientD = clientData.productStarHealths.ToArray();
        //    var list = quickQuoteslist.ToArray();
        //    foreach (var t in tasks)
        //    {
        //        try
        //        {

        //            var postResponse = t; //t.Result would be okay too.
        //            var str = JsonConvert.SerializeObject(list[i]);
        //            ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = productfilter.Inquiry_id, ApiMethod = "QuickQuotes", CompanyName = "Star Health", Request = str, Response = postResponse.Result.ToString() });
        //            StarHealthIndividualQuotesResponse data = JsonConvert.DeserializeObject<StarHealthIndividualQuotesResponse>(postResponse.Result.ToString());
        //            decimal premium = Convert.ToDecimal(data.premium);
        //            data.premium = list[i].BasePremium;
        //            data.serviceTax = Convert.ToString((premium * 18 / 100));
        //            data.BasePremium = premium.ToString();
        //            data.totalPremium = Convert.ToString(premium + (premium * 18 / 100));
        //            data.InsuranceCompany = InsuranceCompany;
        //            data.InsuranceCompanyId = InsuranceCompanyId;
        //            data.logoUrl = logoUrl;
        //            data.schemaId = list[i].schemeId;
        //            data.PolicyType = PolicyType;
        //            data.Suminsured = list[i].suminsured;
        //            data.Plan = list[i].Plan == null ? "" : list[i].Plan;
        //            data.AgeBracket = list[i].AgeBracket;
        //            data.suminsuredId = list[i].suminsuredId;
        //            decimal discount = premium - Convert.ToDecimal(data.premium);
        //            data.Id = list[i].Id;
        //            if (discount == 0 || discount < 0)
        //            {
        //                data.discountPercent = 0.00m;
        //            }
        //            else
        //            {
        //                data.discountPercent = Math.Round((discount / premium) * 100, 2);
        //            }


        //            data.product = new
        //            {
        //                policyTypeName = list[i].policyTypeName,
        //                policyName = list[i].policyName,
        //                postalCode = list[i].postalCode,
        //                period = list[i].period,
        //                //schemeId = list[i].schemeId,
        //                insureds = list[i].insureds,
        //                productDetailId = list[i].productDetailId
        //            };
        //            data.productTerms = new
        //            {

        //            };
        //            data.Id = clientD[i].Id;
        //            data.Inquiry_id = productfilter.Inquiry_id;
        //            if (data.premium != null)
        //                postResponses.Add(data);

        //        }
        //        catch (Exception ex)
        //        {
        //            logger.Log(ex.Message, nameof(SearchProductIndivisual), 86);
        //        }
        //        finally
        //        {
        //            i++;
        //        }
        //    }
        //    logger.Log("Preparing Response", nameof(SearchProductIndivisual), 80);

        //    return postResponses;
        //}

    }

}

﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.Caching;
//using System.Text;
//using System.Threading.Tasks;
//using HealthPlan.BusinessLogic.CacheBussinessManager;

//using HealthPlan.BusinessLogic.ProposalBusinessManager;
//using HealthPlan.Common.Global;
//using HealthPlan.Common.RestApis;
//using HealthPlan.Data.Models.Common;
////using HealthPlan.Data.Models.Manipal.Quote;
////using HealthPlan.Data.Models.Manipal.QuoteFloater;
//using HealthPlan.Data.Models.Product;
//using HealthPlan.Data.Models.Referance;
//using HealthPlan.Data.Models.SearchProduct;
//using HealthPlan.Data.Models.Starhealth;
//using HealthPlan.BusinessLogic.GroupCareBusinessManager;
//using HealthPlan.DataAccessLayer.SearchDataManager;
//using Newtonsoft.Json;

//namespace HealthPlan.BusinessLogic.SearchBusinessManager
//{
//    public class SearchProductFloater
//    {
//        public static async Task<List<StarHealthFloaterQuotesResponse>> QuotesRequest(ProductSearchModel clientData, int pincode, int age, Productfilter productfilter, List<ApiRequestResponseLog> ApiRequestResponseLog)
//        {

//            var tasks = new List<Task<string>>();
//            DateTime currentDate = DateTime.Now;
//            string logoUrl = clientData.productStarHealths.FirstOrDefault().logoUrl;
//            string InsuranceCompany = clientData.productStarHealths.FirstOrDefault().CompanyName;
//            int InsuranceCompanyId = clientData.productStarHealths.FirstOrDefault().CompanyId;
//            string PolicyType = productfilter.policytype;
//            string suminsured = productfilter.Suminsured;
//            List<QuickQuotesModel> quickQuoteslist = new List<QuickQuotesModel>();

//            foreach (var item in clientData.productStarHealths)
//            {
//               // string[] period = item.Period.Split(',', StringSplitOptions.RemoveEmptyEntries);
//               // foreach (var peri in period)
//               // {
//                    QuickQuotesModel quickQuotes = new QuickQuotesModel();

//                    quickQuotes.period = Convert.ToInt32(item.Period);
//                    quickQuotes.APIKEY = AppSettings.StarhealthApikey;
//                    quickQuotes.SECRETKEY = AppSettings.StarhealthApiSecret;
//                    quickQuotes.policyTypeName = item.ProductCode;
//                    quickQuotes.postalCode = pincode;
//                    quickQuotes.policyName = item.ProductName;
//                    quickQuotes.schemeId = item.SchemeId;
//                    quickQuotes.sumInsuredId = item.SumInsuredId;
//                    quickQuotes.productDetailId = item.ProductDetailId;
//                    quickQuotes.suminsured = Convert.ToInt32(item.SumInsured);
//                    quickQuotes.insureds = new List<Basic>();
//                    quickQuotes.Plan = item.Plan;
//                    quickQuotes.AgeBracket = item.AgeBracket;
//                    quickQuotes.BasePremium = item.BasePremium;
//                    //quickQuotes.insureds = new List<Basic>() { new Basic { dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}", buyBackPED = 0 } };
//                    if (productfilter.SelfAge != null && productfilter.SelfAge > 0)
//                    {
//                        DateTime Dob = currentDate.AddYears((int)-productfilter.SelfAge);
//                        Basic insured = new Basic();
//                        insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                        insured.buyBackPED = 0;
//                        quickQuotes.insureds.Add(insured);
//                    }
//                    if (productfilter.SpouseAge != null && productfilter.SpouseAge > 0)
//                    {
//                        DateTime Dob = currentDate.AddYears((int)-productfilter.SpouseAge);
//                        Basic insured = new Basic();
//                        insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                        insured.buyBackPED = 0;
//                        quickQuotes.insureds.Add(insured);
//                    }
//                    if (productfilter.SonAge != null && !(productfilter.SonAge?.Length == 1) && !(productfilter.SonAge?[0] == 0))
//                    {
//                        for (int a = 0; a < productfilter.SonAge?.Length; a++)
//                        {
//                            DateTime Dob = currentDate.AddYears((int)-productfilter.SonAge?[a]);
//                            Basic insured = new Basic();
//                            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                            insured.buyBackPED = 0;
//                            quickQuotes.insureds.Add(insured);

//                        }
//                    }
//                    if (productfilter.DaughterAge != null && !(productfilter.DaughterAge?.Length == 1) && !(productfilter.DaughterAge?[0] == 0))
//                    {
//                        for (int a = 0; a < productfilter.DaughterAge?.Length; a++)
//                        {
//                            DateTime Dob = currentDate.AddYears((int)-productfilter.DaughterAge?[a]);
//                            Basic insured = new Basic();
//                            insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                            insured.buyBackPED = 0;
//                            quickQuotes.insureds.Add(insured);

//                        }
//                    }

//                    if (productfilter.FatherAge != null && productfilter.FatherAge > 0)
//                    {
//                        DateTime Dob = currentDate.AddYears((int)-productfilter.FatherAge);
//                        Basic insured = new Basic();
//                        insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                        insured.buyBackPED = 0;
//                        quickQuotes.insureds.Add(insured);
//                    }
//                    if (productfilter.MotherAge != null && productfilter.MotherAge > 0)
//                    {
//                        DateTime Dob = currentDate.AddYears((int)-productfilter.MotherAge);
//                        Basic insured = new Basic();
//                        insured.dob = $"{Dob.Month}/{Dob.Day}/{Dob.Year}";
//                        insured.buyBackPED = 0;
//                        quickQuotes.insureds.Add(insured);
//                    }

//                    quickQuoteslist.Add(quickQuotes);
//               // }
//            };
//            string Url;
//            RestApi<QuickQuotesModel> rest = new RestApi<QuickQuotesModel>();
//          //  IEnumerable<Api> api = Starhealthcommonmanager.GetApiDetails(1, "Quotes");
//         //   IEnumerable<Api> api = GroupCareCommonManager.GetApiDetails(1, "Quotes");
//            rest.baseaddress = api.FirstOrDefault().BaseAddress;
//            Url = api.FirstOrDefault().Url;
//            foreach (var post in quickQuoteslist)
//            {
//                Task<string> func()
//                {
//                    return rest.PostApi(Url, post);
//                }
//                tasks.Add(func());
//            }

//            await Task.WhenAll(tasks.ToArray());

//            List<StarHealthFloaterQuotesResponse> postResponses = new List<StarHealthFloaterQuotesResponse>();
//            var i = 0;

//            var clientD = clientData.productStarHealths.ToArray();
//            var list = quickQuoteslist.ToArray();
//            foreach (var t in tasks)
//            {
//                try
//                {
//                    var postResponse = t; //t.Result would be okay too.
//                    var requestStr = JsonConvert.SerializeObject(list[i]);
//                    ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = productfilter.Inquiry_id, ApiMethod = "QuickQuotes", CompanyName = "Star Health", Request = requestStr, Response = postResponse.Result.ToString() });
//                    StarHealthFloaterQuotesResponse data = JsonConvert.DeserializeObject<StarHealthFloaterQuotesResponse>(postResponse.Result.ToString());
//                    decimal premium = Convert.ToDecimal(data.premium);
//                    data.premium = list[i].BasePremium;
//                    data.serviceTax = Convert.ToString((premium * 18 / 100));
//                    data.BasePremium = premium.ToString();
//                    data.totalPremium = Convert.ToString(premium + (premium * 18 / 100));
//                    data.InsuranceCompany = InsuranceCompany;
//                    data.InsuranceCompanyId = InsuranceCompanyId;
//                    data.logoUrl = logoUrl;
//                    data.schemaId = list[i].schemeId;
//                    data.PolicyType = PolicyType;
//                    data.Suminsured = list[i].suminsured;
//                    data.suminsuredId = list[i].sumInsuredId;
//                    data.Plan = list[i].Plan == null ? "" : list[i].Plan;
//                    data.AgeBracket = list[i].AgeBracket;
//                    decimal discount = premium - Convert.ToDecimal(data.premium);
//                    if (discount == 0 || discount < 0)
//                    {
//                        data.discountPercent = 0.00m;
//                    }
//                    else
//                    {
//                        data.discountPercent = Math.Round((discount / premium) * 100,2);
//                    }
//                    data.product = new
//                    {
//                        policyTypeName = list[i].policyTypeName,
//                        policyName = list[i].policyName,
//                        postalCode = list[i].postalCode,
//                        period = list[i].period,
//                        //schemeId = list[i].schemeId,
//                        insureds = list[i].insureds,
//                        productDetailId = list[i].productDetailId
//                    };
//                    data.productTerms = new
//                    {
//                    };
//                    data.Inquiry_id = productfilter.Inquiry_id;
//                    data.Id = clientD[i].Id;
//                    if (data.premium != null)
//                        postResponses.Add(data);
//                }
//                catch (Exception ex)
//                {
//                    logger.Log(ex.Message, nameof(SearchProductFloater), 193);
//                }
//                finally
//                {
//                    i++;
//                }
//            }
//            logger.Log("Done Creating Response", nameof(SearchProductFloater), 167);
//            return postResponses;
//        }
//        //public static async Task<List<ManipalQuotesResponse>> manipalQuotesRequest(ProductSearchModel clientData, int pincode, int age, string PolicyType, string Suminsured, List<KeyValuePair<string, int?>> menbers, int Adults, int child, string Inquiry_id, List<ApiRequestResponseLog> ApiRequestResponseLog)
//        //{
//        //    var tasks = new List<Task<string>>();
//        //    DateTime currentDate = DateTime.Now;
//        //    DateTime Dob = currentDate.AddYears(-age);
//        //    string logoUrl = clientData.productManipals.FirstOrDefault().logoUrl;
//        //    string InsuranceCompany = clientData.productManipals.FirstOrDefault().CompanyName;
//        //    int InsuranceCompanyId = clientData.productManipals.FirstOrDefault().CompanyId;
//        //    string Policytype = PolicyType;
//        //    List<QuoteFloaterResquest> ManipalQuatationTO = new List<QuoteFloaterResquest>();


//        //    var requestheader = new List<KeyValuePair<string, string>>() {
//        //                new KeyValuePair<string, string>("app_key", AppSettings.ManipalQuoteskey),
//        //                new KeyValuePair<string, string>("app_id",AppSettings.ManipalQuotesAppId),
//        //        };

//        //    logger.Log("Set Api Headers", nameof(SearchProductFloater), 94);

//        //    //herer is the code of get zone 
//        //    string zone = SearchCache.GetZone(pincode);

//        //    var insureds = new List<dynamic>();
//        //    foreach (KeyValuePair<string, int?> member in menbers)
//        //    {
//        //        insureds.Add(new { dob = member.Value.ToString(), buyBackPED = "" });
//        //    }

//        //    //fnish here

//        //    //  var groupedData = clientData.productManipals.GroupBy(c => c.BASEPLANOPTIONCD);
//        //    foreach (var item in clientData.productManipals)
//        //    {
//        //        //var item = group.FirstOrDefault();

//        //        foreach (var tenure in item.FIXEDTERMVALUES.Split(','))
//        //        {
//        //            List<ListofquotationTOResquest> listofquotationTO = new List<ListofquotationTOResquest>();


//        //            List<QuotationProductDOListResquest> quotationProductDOList = new List<QuotationProductDOListResquest>();
//        //            List<QuotationProductInsuredDOListResquest> quotationProductInsuredDOList = new List<QuotationProductInsuredDOListResquest>();
//        //            ListofquotationTOResquest quotationTO = new ListofquotationTOResquest();
//        //            quotationTO.ID = item.ID;
//        //            quotationTO.quoteId = "";
//        //            quotationTO.channelId = "148";
//        //            quotationTO.longName = item.LONGNAME;
//        //            quotationTO.productId = AppSettings.IsProduction == false ? "RPRT03SB02" : item.PRODUCTID;
//        //            quotationTO.parentProductId = item.PARENTPRODUCTID == null ? "NULL" : item.PARENTPRODUCTID;
//        //            // quotationTO.parentProductVersion = item.PRODUCTVERSION;
//        //            quotationTO.noOfAdults = Adults;
//        //            quotationTO.noOfKids = child;
//        //            quotationTO.tenure = Convert.ToInt32(tenure);
//        //            quotationTO.productPlanOptionCd = item.BASEPLANOPTIONCD;
//        //            quotationTO.policyType = item.COVERTYPECD;//"FAMILYFLOATER";
//        //                                                      // quotationTO.saveFl = "YES";
//        //            quotationTO.quoteTypeCd = "INITIAL";
//        //            quotationTO.quotationDt = $"{DateTime.Now.Day}/{DateTime.Now.Month}/{DateTime.Now.Year}"; //  
//        //            quotationTO.agentId = "1600099-01";
//        //            quotationTO.campaignCd = "DEFCMP03";
//        //            quotationTO.inwardTypeCd = "NEWBUSINESS";
//        //            quotationTO.inwardSubTypeCd = "PROPOSALDOCUMENT";
//        //            quotationTO.astpFlag = "NO";
//        //            quotationTO.saveFl = "Yes";
//        //            quotationTO.policyNum = "";

//        //            decimal sumInsured;
//        //            decimal.TryParse(item.SUMINSUREDVALUE, out sumInsured);
//        //            foreach (KeyValuePair<string, int?> member in menbers)
//        //            {
//        //                QuotationProductInsuredDOListResquest wSQuotationProductInsuredTO = new QuotationProductInsuredDOListResquest
//        //                {
//        //                    issueAge = (int)member.Value,
//        //                    genderCd = member.Key == "SELF" || member.Key == "FATH" || member.Key == "SON" ? "MALE" : "FEMALE",
//        //                    dob = $"{Dob.Day}/{Dob.Month}/{Dob.Year}",//"01/07/1986",
//        //                    zoneCd = zone,
//        //                    relationCd = member.Key,
//        //                    productPlanOptionCd = item.BASEPLANOPTIONCD,//"IN-PRT7.5-HMB500",
//        //                    sumInsured = Convert.ToInt32(item.SUMINSUREDVALUE),//750000
//        //                    annualIncome = 500000
//        //                };
//        //                quotationProductInsuredDOList.Add(wSQuotationProductInsuredTO);
//        //            }




//        //            QuotationProductDOListResquest wSQuotationProducts = new QuotationProductDOListResquest
//        //            {
//        //                productId = AppSettings.IsProduction == false ? "RPRT03SB02" : item.PRODUCTID,//"RPRT03SB02"
//        //                zoneCd = zone,
//        //                productPlanOptionCd = item.BASEPLANOPTIONCD,
//        //                productTypeCd = item.PRODUCTTYPECD,//"SUBPLAN"
//        //                paymentFrequencyCd = "SINGLE",
//        //                //astpLoadingPerc = 0,
//        //                quotationProductBenefitDOList = new List<QuotationProductBenefitDOListResquest> {
//        //             new QuotationProductBenefitDOListResquest{
//        //                    benefitTypeCd= item.DISCOUNTTYPE,
//        //                    benefitId= item.SIBENEFITCODE,
//        //                    amount= 0,
//        //                    productId= AppSettings.IsProduction == false ? "RPRT03SB02" :item.PRODUCTID
//        //                }
//        //             },
//        //                quotationProductInsuredDOList = quotationProductInsuredDOList,
//        //                quotationProductChargeDOList = new List<QuotationProductChargeDOListResquest> { new QuotationProductChargeDOListResquest() },
//        //                quotationProductAddOnDOList = new List<QuotationProductAddOnDOListResquest> {
//        //            new QuotationProductAddOnDOListResquest()
//        //            },

//        //            };
//        //            quotationProductDOList.Add(wSQuotationProducts);

//        //            quotationTO.quotationProductDOList = quotationProductDOList;

//        //            quotationTO.quotationChangeDOList = new List<QuotationChangeDOListResquest> { new QuotationChangeDOListResquest() };
//        //            quotationTO.quotationChargeDOList = new List<QuotationChargeDOListResquest> { new QuotationChargeDOListResquest() };


//        //            listofquotationTO.Add(quotationTO);

//        //            ManipalQuatationTO.Add(new QuoteFloaterResquest { listofquotationTO = listofquotationTO });
//        //        }
//        //    };
//        //    RestApi<QuoteFloaterResquest> rest1 = new RestApi<QuoteFloaterResquest>();
//        //    IEnumerable<Api> api2 = ManipalCommonManager.GetApiDetails(3, "Quotes");
//        //    rest1.baseaddress = api2.FirstOrDefault().BaseAddress;
//        //    string Url = api2.FirstOrDefault().Url;

//        //    //logger.Log($"{rest1.baseaddress}{Url}", nameof(SearchProductFloater), 157);
//        //    // logger.Log($"{JsonConvert.SerializeObject(ManipalQuatationTO.FirstOrDefault())}", nameof(SearchProductFloater), 158);
//        //    foreach (var post in ManipalQuatationTO)
//        //    {
//        //        var str = JsonConvert.SerializeObject(post);
//        //        Task<string> func()
//        //        {
//        //            return rest1.PostApi(Url, post, requestheader);
//        //        }
//        //        tasks.Add(func());
//        //    }
//        //    logger.Log($"wating Api Response. Number of Tasks {tasks.Count}", nameof(SearchProductFloater), 169);
//        //    try
//        //    {
//        //        await Task.WhenAll(tasks.ToArray());
//        //    }
//        //    catch (Exception ex)
//        //    {

//        //    }

//        //    var allProducts = ManipalQuatationTO;
//        //    var i = 0;

//        //    List<PPMC_Rule_ManipalCigna> pPMC_Rule_Manipals = ProductDetailBAL.GetPPMC_Rule_manipal();

//        //    List<ManipalQuotesResponse> postResponses2 = new List<ManipalQuotesResponse>();
//        //    //logger.Log(tasks.FirstOrDefault().Result.ToString(), nameof(SearchProductFloater), 172);
//        //    foreach (var postResponse in tasks)
//        //    {
//        //        try
//        //        {
//        //            ManipalQuotesResponse data = new ManipalQuotesResponse();
//        //            var requestStr = JsonConvert.SerializeObject(allProducts[i]);
//        //            ApiRequestResponseLog.Add(new ApiRequestResponseLog { Inquiry_id = Inquiry_id, ApiMethod = "QuickQuotes", CompanyName = "Manipla", Request = requestStr, Response = postResponse.Result.ToString() });
//        //            QuoteResponse quoteResponse = JsonConvert.DeserializeObject<QuoteResponse>(postResponse.Result.ToString());
//        //            if (quoteResponse != null)
//        //            {

//        //                var firstData = quoteResponse.listofquotationTO.FirstOrDefault();
//        //                var productFirstData = allProducts[i].listofquotationTO.FirstOrDefault();
//        //                data.totalPremium = string.Format("{0:0.00}", firstData?.totPremium).ToString();
//        //                decimal? discountPer = firstData?.quotationProductDOList.FirstOrDefault()?.discount;
//        //                if(discountPer > 0)
//        //                {
//        //                    discountPer = (discountPer / firstData?.quotationProductDOList.FirstOrDefault()?.basePremium) * 100;
//        //                }
//        //                data.discountPercent = Math.Round((decimal)discountPer,2);

//        //                if (data.discountPercent == 0)
//        //                {
//        //                    data.discountPercent = 0.00m;
//        //                }

//        //                data.BasePremium = string.Format("{0:0.00}", firstData?.quotationProductDOList.FirstOrDefault()?.basePremium).ToString();
                       
//        //                if (firstData?.tenure > 1)
//        //                {
//        //                    var basePremium = firstData?.quotationProductDOList.FirstOrDefault()?.basePremium;
//        //                    var premium = basePremium - (basePremium * discountPer / 100);
//        //                    data.premium = string.Format("{0:0.00}", premium).ToString();
//        //                }
//        //                else
//        //                {
//        //                    data.premium = string.Format("{0:0.00}", firstData?.quotationProductDOList.FirstOrDefault()?.basePremium).ToString();
//        //                }
//        //                data.serviceTax = string.Format("{0:0.00}", firstData?.quotationChargeDOList.Sum(u => Convert.ToDouble(u.chargeAmount))).ToString();
//        //                data.InsuranceCompany = InsuranceCompany;
//        //                data.InsuranceCompanyId = InsuranceCompanyId;
//        //                data.logoUrl = logoUrl;
//        //                data.schemaId = 2;
//        //                data.PolicyType = PolicyType;
//        //                data.Suminsured = Convert.ToInt32(firstData?.quotationProductDOList.FirstOrDefault()?.quotationProductInsuredDOList.FirstOrDefault()?.sumInsured);
//        //                data.product = new
//        //                {
//        //                    policyTypeName = firstData?.policyType,
//        //                    policyName = productFirstData.longName,
//        //                    postalCode = pincode,
//        //                    period = firstData?.tenure,
//        //                    //schemeId = 2,
//        //                    insureds = insureds,
//        //                    productDetailId = productFirstData?.productId
//        //                };

//        //                data.quoteId = firstData.quoteId;

//        //                data.Id = (int)(productFirstData?.ID);
//        //                data.Plan = "";

//        //            }

//        //            if (pPMC_Rule_Manipals.Count > 0)
//        //            {
//        //                List<PPMC_Rule_ManipalCigna> pPMC_Rule = pPMC_Rule_Manipals.Where(x => data.Suminsured >= x.SumInsuredMin && data.Suminsured <= x.SumInsuredMax && age >= x.AgeGroupMin && age <= x.AgeGroupMax).ToList();

//        //                data.productTerms = pPMC_Rule;
//        //            }
//        //            data.Inquiry_id = Inquiry_id;
//        //            postResponses2.Add(data);

//        //        }
//        //        catch (Exception ex)
//        //        {
//        //            if (postResponse.Result.ToString().Contains("504 Gateway Time-out"))
//        //            {
//        //                logger.Log(postResponse.Result.ToString(), nameof(SearchProductFloater), 193);
//        //            }
//        //            logger.Log(ex.Message, nameof(SearchProductFloater), 193);

//        //        }
//        //        finally
//        //        {
//        //            i++;
//        //        }
//        //    }
//        //    logger.Log("Done Creating Response", nameof(SearchProductFloater), 189);
//        //    return postResponses2;
//        //}
//        //public static bool ClearCache()
//        //{
//        //    return SearchCache.ResetCache();
//        //}
//    }
//}

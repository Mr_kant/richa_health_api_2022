﻿using HealthPlan.BusinessLogic.CacheBussinessManager;
using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.Product;
using HealthPlan.Data.Models.Referance;
using HealthPlan.DataAccessLayer.MasterDataManager;
using HealthPlan.DataAccessLayer.SearchDataManager;
using System;
using System.Collections.Generic;

namespace HealthPlan.BusinessLogic.SearchBusinessManager
{
    public  class ProductDetailBAL
    {  

        public static ProductSearchModel FilterProduct(string Suminsured,string policytype, int Age, string Schema)
        {
            char[] spearator = { '-' };
            int SumInsuredmin;
            int SumInsuredMax;
            int MinAge, MaxAge;
            ProductSearchModel clientData = new ProductSearchModel();
            string[] strsuminsured = Suminsured.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
            SumInsuredmin = Convert.ToInt32(strsuminsured[0]);
            SumInsuredMax = Convert.ToInt32(strsuminsured[1]);
            MinAge = Age.ToLowerNearesMulti5();
            MaxAge = Age.ToUpperNearesMulti5();

            return SearchCache.SearchProduct(SumInsuredmin, SumInsuredMax, policytype, MinAge, MaxAge, Schema, Age);
           
        }
     

        public static decimal? calculatePermium(int tenure, decimal? permium, int tax)
        {
            permium += permium * tax / 100;
            return tenure * permium;
        }

        public static decimal? calculateTax(int tenure, decimal? permium, int tax)
        {
            var GSTtax = permium * tax / 100;
            return tenure * GSTtax;
        }
      

        public static List<SchemeMaster> Getschemes()
        {
            return ProductDetailDAL.GetAllschemes();
        }

        public static void ApiLog(List<ApiRequestResponseLog> ApiRequestResponseLog) {
            MasterCommonDataManager.ApiLog(ApiRequestResponseLog);
        }
        
        public static bool ClearCache()
        {
            return SearchCache.ResetCache();
        }
    }
}

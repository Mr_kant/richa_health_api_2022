﻿using HealthPlan.DataAccessLayer.AuthenticateDataManager;
using HealthPlan.Data.Models.Authenticate;
using System;
using System.Collections.Generic;
using System.Text;
using HealthPlan.Common.ExtensionMethods;
using System.Data;
using HealthPlan.DataAccessLayer.Common;
using Dapper;
using System.Linq;

namespace HealthPlan.BusinessLogic.AuthenticateBusinessManager
{
    public class AuthenticateBAL
    {
      
        public static ResponseModel Authenticate_User (LoginModel model)
        {
            string password = null;
            model.Password = model.Password.Base64Encode();
            return AuthenticateDAL.Authenticate_User(model);
        }

        public static ResponseModel AddUser(AddUserModel model)
        {
            string password = null;
            model.Password = model.Password.Base64Encode();
            return AuthenticateDAL.AddUser(model);
        }
        //public static ResponseModel Authenticate_User(LoginModel model)
        //{
        //    ResponseModel response = new ResponseModel();
        //    using (var db = Connection.Open())
        //    {
        //        response = db.Query<ResponseModel>("sp_AuthenticateUser", model, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //    }

        //    return response;

        //}
        //public static ResponseModel AddUser(AddUserModel model)
        //{

        //    ResponseModel response = new ResponseModel();
        //    using (var db = Connection.Open())
        //    {

        //        try
        //        {
        //            response = db.Query<ResponseModel>("sp_AddNewuser", model, commandType: CommandType.StoredProcedure).FirstOrDefault();
        //        }
        //        catch (Exception ex)
        //        {
        //            response.code = 0;
        //            response.message = ex.Message;
        //        }
        //    }
        //    return response;

        //}
    }
}

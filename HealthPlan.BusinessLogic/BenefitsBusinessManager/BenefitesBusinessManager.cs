﻿using HealthPlan.BusinessLogic.CacheBussinessManager;
using HealthPlan.Data.Models.Benefits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealthPlan.BusinessLogic.BenefitsBusinessManager
{
	public class BenefitesBusinessManager
	{
		public static BenefitsResponse GetBenefits(BenefitsRequest benefitsRequest)
		{
			var benefitData = BenefitesCache.GetBenefits(benefitsRequest);
			if (benefitData.destriptions.Count == 0) {
				return new BenefitsResponse();
			}
			var categories = benefitData.tableMenu.GroupBy(e => e.CategoryID).ToList();
			var ListCategory = new BenefitsResponse();
			ListCategory.Categories = new List<Category>();
			foreach (var category in categories)
			{
				var tempCategory = new Category();
				tempCategory.Menus = new List<Menu>();
				foreach (var value in category)
				{
					tempCategory.CategoryName = value.Category;
					tempCategory.CategoryID = value.CategoryID;
					//tempCategory.Description = value.Description;
					var menu = new Menu();
					menu.MenuName = value.Menu;
					menu.MenuID = value.MenuID;
					menu.Destriptions = value.Description;
					var keyArray = value.Menu.Split(' ');
					var keyT = string.Join('_', keyArray);
					if (keyT.Contains('-')) 
					{
						keyArray = keyT.Split('-');
						keyT = string.Join('_', keyArray);
					}
					try
					{
						menu.Details = benefitData.destriptions.Select(e => e.GetType().GetProperty(keyT).GetValue(e)).FirstOrDefault().ToString();
					}
					catch (Exception e) { }
					tempCategory.Menus.Add(menu);

				}

			
				ListCategory.Categories.Add(tempCategory);
				ListCategory.Productid = benefitsRequest.Productid;
				ListCategory.CompanyId = benefitsRequest.CompanyId;
				ListCategory.SumInsured = benefitsRequest.SumInsured;
				ListCategory.Plan = benefitsRequest.Plan;
				ListCategory.LogoUrl = benefitData.CompanyDetails.LogoUrl;
				ListCategory.InsuranceCompany = benefitData.CompanyDetails.CompanyName;
				ListCategory.ProductName = benefitData.ProductName;
			}
			return ListCategory;

		}

		public static void ResetCache() {
			BenefitesCache.ResetCache();
		}
	}
}

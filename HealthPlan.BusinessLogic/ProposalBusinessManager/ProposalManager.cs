﻿using HealthPlan.BusinessLogic.CacheBussinessManager;
using HealthPlan.BusinessLogic.GroupCareBusinessManager;
using HealthPlan.Common.Global;
using HealthPlan.Data.Models.Authenticate;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.GeneralProposal;
using HealthPlan.Data.Models.GroupCare;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HealthPlan.BusinessLogic.ProposalBusinessManager
{
    public class ProposalManager
    {
        public string Error { get; private set; }


        public ProposalManager()
        {
            Error = string.Empty;
        }


        //----------------------------------------------------------------------------------------------------------//
        public async Task<ResponseWithUrl<ProposalResponse>> Proposal(GeneralProposal proposal, List<ApiRequestResponseLog> ApiRequestResponseLog)
        {
            ResponseWithUrl<ProposalResponse> responseWithUrl = new ResponseWithUrl<ProposalResponse>();
            IEnumerable<GroupCarePartyRelation> groupCarePartyRelation = GroupCareCommonManager.GetRelationShip();


            if (proposal.InsuranceCompanyId == 1) //"Religare / care health"
            {

                if ((proposal.PolicyType).ToLower() == "individual")
                {
                    IEnumerable<GroupCareSuminsured> SuminsuredDetails = GroupCareCommonManager.GetSuminsureds(proposal.SumInsured, 1); // 1 is used for Individual
                    GroupQuotationInd QuotationInd = new GroupQuotationInd();
                    QuotationInd.intPolicyDataIO = new GroupCare.IntPolicyDataIO(proposal, SuminsuredDetails);
                    responseWithUrl.Data = await GroupCareProposal.GroupCareProposalInd(QuotationInd, ApiRequestResponseLog, proposal.Inquiry_id);

                    

                    if (responseWithUrl.Data != null)
                    {
                        responseWithUrl.Data.serviceTax = proposal.serviceTax;
                        responseWithUrl.Data.premium = proposal.premium;
                        responseWithUrl.Data.productDetailId = proposal.ProductDetailId;
                        responseWithUrl.Data.CompanyId = proposal.InsuranceCompanyId;
                        //responseWithUrl.Data.Id = proposal.Id;

                    }
                }
                else
                {
                    IEnumerable<GroupCareSuminsured> SuminsuredDetails = GroupCareCommonManager.GetSuminsureds(proposal.SumInsured, 2); // 2 is used for Floater
                    GroupQuotationInd groupQuotationInd = new GroupQuotationInd();
                    groupQuotationInd.intPolicyDataIO = new GroupCare.IntPolicyDataIO(proposal, true, groupCarePartyRelation, SuminsuredDetails);
                    responseWithUrl.Data = await GroupCareProposal.GroupCarePerposalFloat(groupQuotationInd, ApiRequestResponseLog, proposal.Inquiry_id);
                    if (responseWithUrl.Data != null)
                    {
                        responseWithUrl.Data.serviceTax = proposal.serviceTax;
                        responseWithUrl.Data.premium = proposal.premium;
                        responseWithUrl.Data.productDetailId = proposal.ProductDetailId;
                        responseWithUrl.Data.CompanyId = proposal.InsuranceCompanyId;
                        //responseWithUrl.Data.Id = proposal.Id;
                    }
                }
                if (responseWithUrl.Data != null)
                {
                    responseWithUrl.Data.productDetailId = proposal.ProductDetailId;
                    responseWithUrl.Urls = $"{AppSettings.DomianUrl }/api/Care/GetPaymentUrl/{responseWithUrl.Data.ProposalNum}";
                }

                Error = GroupCareProposal.GetError();

            }


            if (responseWithUrl.Data != null)
            {
                responseWithUrl.Data.Inquiry_id = proposal.Inquiry_id;
                responseWithUrl.message = "Success";
                return responseWithUrl;
            }
            else
            {
                responseWithUrl.message = Error;
                return responseWithUrl;
            }


        }


    }
}

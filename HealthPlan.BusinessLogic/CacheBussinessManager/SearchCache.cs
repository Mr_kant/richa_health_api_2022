﻿using HealthPlan.Common.Cache;
using HealthPlan.Data.Models.Product;
using HealthPlan.DataAccessLayer.SearchDataManager;

namespace HealthPlan.BusinessLogic.CacheBussinessManager
{
    public class SearchCache
    {
        private static string ManipalSearch = "SearchProduct";
        private static string Zone = "zone";
        public static ProductSearchModel SearchProduct(int SumInsuredmin, int SumInsuredMax, string policytype, int minAge, int maxAge, string Schema, int Age)
        {
            var cacheKey = $"{ManipalSearch}-{SumInsuredmin}-{SumInsuredMax}-{policytype}-{minAge}-{maxAge}-{Schema}";

            var cachedData = ObjectCacheUtil.Get<ProductSearchModel>(cacheKey);
            cachedData = ProductDetailDAL.FilterProduct(SumInsuredmin, SumInsuredMax, policytype, minAge, maxAge, Schema, Age);
            ObjectCacheUtil.Set(cacheKey, cachedData);
            return cachedData;
        }

        public static string GetZone(int pincode)
        {
            var cacheKey = $"{Zone}-{pincode}";
            var cachedData = ObjectCacheUtil.Get<string>(cacheKey);
            if (cachedData == null)
            {
                cachedData = ProductDetailDAL.GetZone(pincode);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }

            return cachedData;

        }
        public static bool ResetCache()
        {
            ObjectCacheUtil.RemoveBySubString(ManipalSearch);
            return true;
        }
    }
}

﻿using HealthPlan.Common.Cache;
using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Data.Models.Referance;
using HealthPlan.DataAccessLayer.ReligareDataManager;
using System.Collections.Generic;

namespace HealthPlan.BusinessLogic.CacheBussinessManager
{
    public static class  ReligareCache
	{
        private static string ReligarePolicyPDFDetails = "Religare-PolicyPDFDetails";

        //public static CareProducts GetCareProductInfo(int ProductDetailId)
        //{
        //    var cacheKey =$"{ReligareSumInsured}-{ProductDetailId}";

        //    var cachedData = ObjectCacheUtil.Get<CareProducts>(cacheKey);

        //    if (cachedData == null)
        //    {
        //        cachedData = ReligareCommonDataManager.GetCareProduct(ProductDetailId);
        //        ObjectCacheUtil.Set(cacheKey, cachedData);
        //    }

        //    return cachedData;


        //}
        //public static IEnumerable<PartyRelation> GetRelationShipInfo()
        //{
        //    var cacheKey = $"{ReligareRelationShipInfo}";

        //    var cachedData = ObjectCacheUtil.Get<IEnumerable<PartyRelation>>(cacheKey);

        //    if (cachedData.IsEnumerableNullOrEmpty())
        //    {
        //        cachedData = ReligareCommonDataManager.GetRelationShipInfo();
        //        ObjectCacheUtil.Set(cacheKey, cachedData);
        //    }

        //    return cachedData;


        //}



        //public static IEnumerable<Question> GetQuestion(int productId)
        //{
        //    var cacheKey = ReligareQuestion;

        //    var cachedData = ObjectCacheUtil.Get<IEnumerable<Question>>(cacheKey);

        //    if (cachedData.IsEnumerableNullOrEmpty())
        //    {
        //        cachedData = ReligareCommonDataManager.GetQuestion(productId);
        //        ObjectCacheUtil.Set(cacheKey, cachedData);
        //    }

        //    return cachedData;


        //}
        //public static IEnumerable<Api> GetApiDetails(string baseProductCode, string UrlType)
        //{
        //    var cacheKey = $"{ReligareApiDetails}-{ baseProductCode }-{UrlType}" ;

        //    var cachedData = ObjectCacheUtil.Get<IEnumerable<Api>>(cacheKey);

        //    if (cachedData.IsEnumerableNullOrEmpty())
        //    {
        //        cachedData = ReligareCommonDataManager.GetApiInfo(baseProductCode, UrlType);
        //        ObjectCacheUtil.Set(cacheKey, cachedData);
        //    }

        //    return cachedData;


        //}
        public static IEnumerable<Api> GetApiPolicyPDFDetails(int CompanyId, string UrlType)
        {
            var cacheKey = $"{ReligarePolicyPDFDetails}-{CompanyId}-{UrlType}";

            var cachedData = ObjectCacheUtil.Get<IEnumerable<Api>>(cacheKey);

            if (cachedData.IsEnumerableNullOrEmpty())
            {
                cachedData = ReligareCommonDataManager.GetApiPolicyPDFDetails(CompanyId, UrlType);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }

            return cachedData;


        }

       

        public static bool ResetCache()
        {
            ObjectCacheUtil.RemoveBySubString("Religare-");
            return true;
        }

    }
}

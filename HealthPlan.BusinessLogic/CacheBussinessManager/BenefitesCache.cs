﻿using HealthPlan.Common.Cache;
using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Data.Models.Benefits;
using HealthPlan.DataAccessLayer.MasterDataBenefits;
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.BusinessLogic.CacheBussinessManager
{
	public class BenefitesCache
	{
        private static string BenefiteKey = "Benefite";


        public static BenefitsType GetBenefits(BenefitsRequest benefitsRequest)
        {
            var cacheKey = $"{BenefiteKey}_{benefitsRequest.CompanyId}_{benefitsRequest.Productid}_{benefitsRequest.SumInsured}_{benefitsRequest.Plan}";

            var cachedData = ObjectCacheUtil.Get<BenefitsType>(cacheKey);

            if (cachedData == null)
            {
                cachedData = MasterDataBenefits.GetBenefits(benefitsRequest);
                ObjectCacheUtil.Set(cacheKey, cachedData);
            }

            return cachedData;


        }

        public static bool ResetCache()
        {

            ObjectCacheUtil.RemoveBySubString(BenefiteKey);
            return true;
        }
    }
}

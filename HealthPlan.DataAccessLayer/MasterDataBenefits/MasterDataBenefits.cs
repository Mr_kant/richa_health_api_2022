﻿using Dapper;
using HealthPlan.Data.Models.Benefits;
using HealthPlan.DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealthPlan.DataAccessLayer.MasterDataBenefits
{
	public class MasterDataBenefits
	{
		public static BenefitsType GetBenefits(BenefitsRequest benefitsRequest) {
            BenefitsType benefitsType = new BenefitsType();

            using (var db = Connection.Open())
            {
                using (var multi = db.QueryMultiple("sp_GetBenefits", new
                {
                    benefitsRequest.CompanyId,
                    benefitsRequest.Productid,
                    benefitsRequest.SumInsured,
                    benefitsRequest.Plan
                }, commandType: System.Data.CommandType.StoredProcedure))
                {
                    benefitsType.tableMenu = multi.Read<TableMenu>().ToList();
                    benefitsType.destriptions = multi.Read<Destription>().ToList();
                    benefitsType.CompanyDetails = multi.Read<CompanyItems>().FirstOrDefault();
                    benefitsType.ProductName = multi.Read<string>().FirstOrDefault();
                }
            }
            return benefitsType;
        }
	}
}

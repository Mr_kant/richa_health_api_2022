﻿using Dapper;
using HealthPlan.DataAccessLayer.Common;
using HealthPlan.Data.Models.Product;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using HealthPlan.Data.Models.Religare;
using HealthPlan.Data.Models.SearchProduct;
using HealthPlan.Data.Models.Referance;
using HealthPlan.Data.Models.GroupCare;

namespace HealthPlan.DataAccessLayer.SearchDataManager
{
    public class ProductDetailDAL
    {
        public static ProductSearchModel FilterProduct(int SumInsuredmin, int SumInsuredMax, string policytype, int MinAge, int MaxAge, string Scheme, int Ages)
        {
            ProductSearchModel clientData = new ProductSearchModel();
            string Age = Convert.ToString(Ages);
            using (var db = Connection.Open())
            {
                using (var multi = db.QueryMultiple("sp_productsdetailsSearch", new
                {
                    SumInsuredmin,
                    SumInsuredMax,
                    policytype,
                    MinAge,
                    MaxAge,
                    Age,
                    Scheme,
                }, commandType: System.Data.CommandType.StoredProcedure))
                {

                    clientData.productGroupCare = multi.Read<GroupCareQuotesResponse>().ToList();
                }
            }
            return clientData;
        }


        public static string GetZone(int pincode)
        {
            string zone = null;
            using (var db = Connection.Open())
            {
                const string command = @"select ZONECODE from tbl_PinCodeCityStateMap_ManipalCigna  where PINCODE= @pincode";
                zone = db.Query<string>(command, new
                {
                    pincode

                }).FirstOrDefault();
            }
            return zone;
        }
      
        public static List<SchemeMaster> GetAllschemes()
        {
            List<SchemeMaster> sechems = new List<SchemeMaster>();
            using (var db = Connection.Open())
            {
                const string command = @"select * from tbl_SchemeMaster";
                sechems = db.Query<SchemeMaster>(command).ToList();
            }
            return sechems;
        }
    }
}

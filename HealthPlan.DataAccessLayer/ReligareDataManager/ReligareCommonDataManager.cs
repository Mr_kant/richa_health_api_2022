﻿using HealthPlan.Data.Models.Religare;
using System;
using System.Collections.Generic;
using System.Text;
using HealthPlan.DataAccessLayer.Common;
using Dapper;
using HealthPlan.Data.Models.Authenticate;
using HealthPlan.Data.Models.Referance;
using HealthPlan.Data.Models.GeneralProposal;
using System.Data;
using System.Linq;

namespace HealthPlan.DataAccessLayer.ReligareDataManager
{
    public class ReligareCommonDataManager 
    {
       
        public static IEnumerable<Question> GetQuestion(int? productId)
        {
            ResponseModel response = new ResponseModel();
            IEnumerable<Question> Questionlst = new List<Question>();
            using (var db = Connection.Open())
            {
                Questionlst = db.Query<Question>("sp_Get_Question_Religare", new { productId }, commandType: CommandType.StoredProcedure);


            }

            return Questionlst;

        }
        
        public static IEnumerable<Api> GetApiPolicyPDFDetails(int CompanyId, string UrlType)
        {

            IEnumerable<Api> api = new List<Api>();
            using (var db = Connection.Open())
            {
                const string command = @"select * from tbl_Api_GroupCare360 where CompanyId=@CompanyId and UrlType =@UrlType";
                api = db.Query<Api>(command, new
                {
                    CompanyId,
                    UrlType
                });
            }

            return api;

        }



    }
}

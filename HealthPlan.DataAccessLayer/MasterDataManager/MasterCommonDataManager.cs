﻿using Dapper;
using HealthPlan.Data.Models.Common;
using HealthPlan.DataAccessLayer.Common;
using System.Collections.Generic;

namespace HealthPlan.DataAccessLayer.MasterDataManager
{
    public class MasterCommonDataManager
    {
        public static int ApiLog(List<ApiRequestResponseLog> apiRequestResponseLogs)
        {
            int result = -1;
            string insertQuery = $@"
                            INSERT INTO tbl_BaseCompany_RequestResponse 
                            (
                               Inquiry_id,
                               CompanyName,
                               Request,
                               Response,
                               ApiMethod,
                                LogDate
                            )
                            VALUES
                            (
                                @Inquiry_id,
                                @CompanyName,
                                @Request,
                                @Response,
                                @ApiMethod,
                                getdate()
                            ) 
                        ";


            using (var db = Connection.Open())
            {
                result = db.Execute(insertQuery, apiRequestResponseLogs);
            }

            return result;
        }

    }
}



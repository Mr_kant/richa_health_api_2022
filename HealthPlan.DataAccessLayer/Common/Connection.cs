﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace HealthPlan.DataAccessLayer.Common
{
	public class Connection
	{
		public static string ConnectionString { get; set; }

		public static IDbConnection Open()
		{
			IDbConnection connection = new SqlConnection(ConnectionString);
			connection.Open();
			return connection;
		}
	}
}

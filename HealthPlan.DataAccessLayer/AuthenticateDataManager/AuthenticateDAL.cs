﻿using Dapper;
using HealthPlan.DataAccessLayer.Common;
using HealthPlan.Data.Models.Authenticate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace HealthPlan.DataAccessLayer.AuthenticateDataManager
{
    public class AuthenticateDAL
    {
        public static ResponseModel Authenticate_User(LoginModel model)
        {
            ResponseModel response= new ResponseModel();
            using ( var db = Connection.Open())
            {
                response = db.Query<ResponseModel>("sp_AuthenticateUser", model, commandType: CommandType.StoredProcedure).FirstOrDefault();
            }

            return response;

        }
        public static ResponseModel AddUser(AddUserModel model)
        {
            
            ResponseModel response = new ResponseModel();
            using (var db = Connection.Open())
            {
               
                try
                {
                    response = db.Query<ResponseModel>("sp_AddNewuser", model, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
                catch(Exception ex)
                {
                    response.code = 0;
                    response.message = ex.Message;
                }
            }
            return response;

        }
    }
}

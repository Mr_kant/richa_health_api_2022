﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Common.Global
{
    public class AppSettings
    {
        public static string ReligareAppId { get; set; }
        public static string ReligareSignature { get; set; }
        public static string ReligareTimeStamp { get; set; }
        public static string LogPath { get; set; }
        public static bool IsProduction { get; set; }
        public static string DomianUrl { get; set; }

        public static string BaseAgentId { get; set; }

    }
}

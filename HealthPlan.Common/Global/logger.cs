﻿using HealthPlan.Common.Global;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HealthPlan.Common.Global
{
	public class logger
	{

		static StringBuilder message = new StringBuilder();
		public static void Log(string Message, string nameSpaceClass, int lineNumber)
		{
			message.AppendLine($"{nameSpaceClass}  Line No: {lineNumber}  {Message}");
		}
		public static void Log()
		{
			string path = Environment.CurrentDirectory.ToString();
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
			string filepath = path + "\\Insurance_Api_Log\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
			if (!File.Exists(filepath))
			{
				// Create a file to write to.   
				using (StreamWriter sw = File.CreateText(filepath))
				{
					sw.WriteLine(message.ToString());
				}
			}
			else
			{
				using (StreamWriter sw = File.AppendText(filepath))
				{
					sw.WriteLine(message.ToString());
				}
			}

			message = new StringBuilder();
		}
		public static void CreateLogFile(string Res, string module)
		{
			string newFileName = module + DateTime.Now.ToString("hh_mm_ss");

			string activeDir = "c:\\Insurance_Api_Log\\" + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";

			try
			{
				DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
				if (!Directory.Exists(objDirectoryInfo.FullName))
				{
					Directory.CreateDirectory(activeDir);
				}
				try
				{
					string path = activeDir + newFileName + ".txt";
					if (!File.Exists(path))
					{
						using (StreamWriter sw = File.CreateText(path))
						{
							sw.Write(Res);
						}
					}
				}
				catch (Exception ex)
				{
				}
			}
			catch (Exception ex)
			{
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace HealthPlan.Common.ExtensionMethods
{
   public static class Base64Extension
    {
        public static string Base64Encode(this string data)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(data);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static MemoryStream ConvertBase64ToStream(this string base64string)
        {
            int mod4 = base64string.Length % 4;
            if (mod4 > 0)
            {
                base64string += new string('=', 4 - mod4);
            }
            byte[] byteData = Convert.FromBase64String(base64string);
            MemoryStream stream = new MemoryStream();
            stream.Write(byteData, 0, byteData.Length);
            stream.Position = 0;
            return stream;
        }
    }
}

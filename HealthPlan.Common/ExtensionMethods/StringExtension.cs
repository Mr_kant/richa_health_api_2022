﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HealthPlan.Common.ExtensionMethods
{
    public static class  StringExtension
    {
        public static List<string> ToList( this string data)
        {
            if (data.Length > 1)
                return data.Split(',').ToList();
            else
                return null;
        }

        public static bool IsEnumerableNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }
        
            public static bool ContainsInvariant(this string sourceString,  string filter)
            {
                return sourceString.ToLowerInvariant().Contains(filter);
            }
        
    }
}

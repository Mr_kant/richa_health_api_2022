﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace HealthPlan.Common.ExtensionMethods
{
	public static class DateFormater
	{
		public static string StarDate(this DateTime date)
		{

			//"dob": "July 17, 1994",
			var Day = date.Day;
			var Month = date.ToMonthName();
			var Year = date.Year;

			return $"{Month} {Day}, {Year}";

		}

		public static string CareDate(this DateTime date)
		{

			// "birthDt": "28/01/1997",
			var Day = date.Day;
			var Month = date.Month;
			var Year = date.Year;
			var day = Day < 10 ? "0" + Day : Day.ToString();
			var month = Month < 10 ? "0" + Month : Month.ToString();


			return $"{day}/{month}/{Year}";
			//return date.ToString("dd/MM/yyyy");

		}


		public static string ManipalDate(this DateTime date)
		{

			// policyExpiryDt": "18/08/2023",
			return date.ToString("dd/MM/yyyy");

		}

		public static string ToMonthName(this DateTime dateTime)
		{
			return CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateTime.Month);
		}

		public static string ToShortMonthName(this DateTime dateTime)
		{
			return CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(dateTime.Month);
		}
	}
}

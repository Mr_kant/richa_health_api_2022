﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Common.ExtensionMethods
{
   public static class IntExtension
    {
        public static int ToLowerNearesMulti5(this int data)
        {
            if (data >= 5)
            {
                int quotient = data / 5;
                return quotient * 5;
            }
            else
            {
                return 0;
            }
        }

        public static int ToUpperNearesMulti5(this int data)
        {
            if (data >= 5)
            {
                int quotient = data / 5;
                return quotient * 5 + 5;
            }
            else
            {
                return 0;
            }
        }
    }
}

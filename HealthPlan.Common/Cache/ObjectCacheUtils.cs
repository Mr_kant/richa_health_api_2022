﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;

namespace HealthPlan.Common.Cache
{
    public static class ObjectCacheUtil
    {
        private static ObjectCache objectCache;
        private static ObjectCache ObjectCache
        {
            get
            {
                if (objectCache == null)
                {
                    objectCache = MemoryCache.Default;
                }
                return objectCache;
            }
        }

        private static CacheItemPolicy policy;
        private static CacheItemPolicy GetPolicy(DateTime? expiry = null)
        {
            if (policy == null)
            {
                policy = new CacheItemPolicy
                {
                    SlidingExpiration = ObjectCache.NoSlidingExpiration,
                    Priority = CacheItemPriority.Default
                };
            }
            policy.AbsoluteExpiration = expiry ?? ObjectCache.InfiniteAbsoluteExpiration;

            return policy;
        }

        public static T Get<T>(string key) where T : class
        {
            if (ObjectCache.Get(key) != null)
            {
                return (T)ObjectCache.Get(key);
            }
            return null;
        }

        public static string[] GetKeys(string subStringKey)
        {
            return ObjectCache.Where(o => o.Key.Contains(subStringKey)).Select(s => s.Key).ToArray();
        }

        public static T GetValue<T>(string key)
        {
            if (ObjectCache.Get(key) != null)
            {
                return (T)ObjectCache.Get(key);
            }
            return default(T);
        }

        public static void Set<T>(string key, T value, DateTime? expiry = null)
        {
            if (expiry.HasValue)
            {
                ObjectCache.Set(key, value, GetPolicy(expiry));
            }
            else
            {
                ObjectCache.Set(key, value, ObjectCache.InfiniteAbsoluteExpiration);
            }
        }

        public static void Remove(string key)
        {
            ObjectCache.Remove(key);
        }

        public static void RemoveAll()
        {
            var cacheKeys = ObjectCache.Select(kvp => kvp.Key).ToList();
            foreach (string key in cacheKeys)
            {
                ObjectCache.Remove(key);
            }
        }

        public static void RemoveBySubString(string subString)
        {
            var cacheKeys = GetKeys(subString).ToList();
            foreach (string key in cacheKeys)
            {
                ObjectCache.Remove(key);
            }
        }

        public static bool Exists(string key)
        {
            return (ObjectCache.Get(key) != null);
        }
    }
}

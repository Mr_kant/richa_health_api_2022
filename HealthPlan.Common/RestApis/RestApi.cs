﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HealthPlan.Common.RestApis
{
    public  class RestApi<T> where T : class
    {
        public string baseaddress { get; set; }
        public bool status { get; private set; }
        public HttpStatusCode statusCode { get; private set; }
        public T Data { get; private set; }
        public static RestApi<T> apirequestdata(T data)
        {
            return new RestApi<T>
            {
                Data = data,
            };
        }
        public void CreateHeader(HttpClient header, List<KeyValuePair<string, string>> requestheader)
        {
            header.DefaultRequestHeaders.Accept.Clear();
            foreach (var heade in requestheader)
            {
                header.DefaultRequestHeaders.Add(heade.Key, heade.Value);
            }
            header.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }
        public async Task<string> GetApi(string mainurl, List<KeyValuePair<string, string>> requestheader)
        {
            string result = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseaddress);
                CreateHeader(client, requestheader);
                //GET Method  
                HttpResponseMessage response = await client.GetAsync(mainurl);
                if (response.IsSuccessStatusCode)
                {
                    result = await response.Content.ReadAsStringAsync();

                }
                else
                {
                    //  return response.StatusCode;
                }
                return result;
            }

        }
        public async Task<string> GetApi(string mainurl)
        {
            string result = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(baseaddress);
                // CreateHeader(client, requestheader);
                //GET Method  
                HttpResponseMessage response = await client.GetAsync(mainurl);
                status = response.IsSuccessStatusCode;
                statusCode = response.StatusCode;
                return await response.Content.ReadAsStringAsync();
            }

        }
        public async Task<string> PostApi(string mainurl, List<KeyValuePair<string, string>> requestheader)
        {
            resetValues();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseaddress);
                CreateHeader(client, requestheader);
                HttpResponseMessage response = await client.GetAsync(mainurl);
                status = response.IsSuccessStatusCode;
                statusCode = response.StatusCode;
                return await response.Content.ReadAsStringAsync();
            }
        }
        public  async Task<string> PostApi(string mainurl, T model, List<KeyValuePair<string, string>> requestheader)
        {
            resetValues();
            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = TimeSpan.FromMinutes(30);
                    client.BaseAddress = new Uri(baseaddress);
                    CreateHeader(client, requestheader);
                    HttpResponseMessage response = await client.PostAsJsonAsync(mainurl, model);
                    status = response.IsSuccessStatusCode;
                    statusCode = response.StatusCode;
                    return await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                status = false;
                statusCode = HttpStatusCode.RequestTimeout;
                return ex.Message;
            }
        }        
        public async Task<string> PostApi(string mainurl, T model)
        {
            resetValues();
            string result = null;
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(20);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(baseaddress);
                HttpResponseMessage response = await client.PostAsJsonAsync(mainurl, model);
                status = response.IsSuccessStatusCode;
                statusCode = response.StatusCode;
                return await response.Content.ReadAsStringAsync();
            }
        }
        private void resetValues()
        {
            status = false;
            statusCode = HttpStatusCode.OK;

        }
    }   
}


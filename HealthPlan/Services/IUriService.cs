﻿using HealthPlan.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthPlan.Services
{
	public interface IUriService
	{
		public Uri GetPageUri(PaginationFilter filter, string route);
		public string GetUrl();
	}
}

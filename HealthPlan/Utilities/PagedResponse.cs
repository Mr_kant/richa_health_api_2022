﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthPlan.Utilities
{
	public class PagedResponse<T> : CustomResponse<T>
	{

        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Uri FirstPage { get; set; }
        public Uri LastPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalRecords { get; set; }
        public Uri NextPage { get; set; }
        public Uri PreviousPage { get; set; }
        public PagedResponse(T data, int pageNumber, int pageSize, string message= "Success")
        {
            this.PageNumber = pageNumber;
            this.PageSize = pageSize;
            this.Response = data;
            this.Message = message;
            this.Success = true;
        }
    }
}

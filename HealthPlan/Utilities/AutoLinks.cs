﻿
using HealthPlan.BusinessLogic.ProposalBusinessManager;
using HealthPlan.Data.Models.GroupCare;
using HealthPlan.Data.Models.Product;
using HealthPlan.Data.Models.Referance;
using Microsoft.AspNetCore.Razor.Language;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthPlan.Utilities
{
	public class AutoLinks
	{
		private static string baseUrl = "";

		public static ProductSearchModel AddNextSearchUrls(ProductSearchModel productSearchModel)
		{
			productSearchModel.productGroupCare = ReligaresSearch(productSearchModel.productGroupCare);

			return productSearchModel;
		}
	


		public static dynamic GetPaymentRedirectionTokenUrl(string referenceId, int productDetailId) 
		{
			var controllerName = "StarHealth";
			dynamic urlList = new
			{
				PaymentRedirectionTokenUrl = GenerateURL(controllerName, "GetPaymentRedirectionToken", $"{referenceId}/{productDetailId}")
			};
			return urlList;
		}

		public static dynamic StarHealthQuickQuote(string ProductName)
		{

			var controllerName = "StarHealth";
			var methodName = "Proposal";
			if (methodName == "Proposal")
			{
				if (ProductName.IndexOf("Floater") > 0)
				{
					methodName = "PerposalFloaters";
				}
				else
				{
					methodName = "ProposalIndividual";
				}
			}


			dynamic urlList = new
			{
				GetCity = GenerateURL(controllerName, "GetCity", "<pincode>"),
				GetArea = GenerateURL(controllerName, "GetArea", "<city_id>"),
				GetGst = GenerateURL(controllerName, "GetGst", string.Empty),
				GetCountry = GenerateURL(controllerName, "GetCountry", string.Empty),
				GetState = GenerateURL(controllerName, "GetState", string.Empty),
				GetOccupation = GenerateURL(controllerName, "GetOccupation", string.Empty),
				GetRelationship = GenerateURL(controllerName, "GetRelationship", string.Empty),
				Proposal = GenerateURL(controllerName, methodName, string.Empty)
			};


			return urlList;

		}

		


		private static string GenerateURL(string controllerName, string methods, string param) {

			string url = $"{baseUrl}/api/{controllerName}/{methods}";
			if (param != "")
			{
				url = $"{url}/{param}";
			}
			return url;
			
		}

		// Code for religare
		private static List<GroupCareQuotesResponse> ReligaresSearch(List<GroupCareQuotesResponse> productReligare)
		{
			foreach (GroupCareQuotesResponse productreligare in productReligare)
			{

				var controllerName = "Religare";
				var methodName = "Perposal";

				
				dynamic urlList = new
				{
					GetSumInsured = GenerateURL(controllerName, "GetSumInsured", productreligare.ProductDetailID.ToString()),
					GetRelationShip = GenerateURL(controllerName, "GetRelationShip", productreligare.ProductDetailID.ToString()),
					GetQuestion = GenerateURL(controllerName, "GetQuestion", string.Empty),
					Perposal = GenerateURL(controllerName, methodName, string.Empty)
				};
				productreligare.logoUrl = urlList;
			}

			return productReligare;

		}

	}
}

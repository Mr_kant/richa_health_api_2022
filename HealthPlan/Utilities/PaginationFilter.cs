﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthPlan.Utilities
{
	public class PaginationFilter
	{
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OtherFilters { get; set; }
        public PaginationFilter()
        {
            this.PageNumber = 1;
            this.PageSize = 100;
        }
        public PaginationFilter(int pageNumber, int pageSize, string OtherFilters = "")
        {
            this.PageNumber = pageNumber < 1 ? 1 : pageNumber;
            this.PageSize = pageSize > 100 ? 100 : pageSize;
            this.OtherFilters = OtherFilters;
        }
    }
}

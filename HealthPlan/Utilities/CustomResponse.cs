﻿

using HealthPlan.Data.Models.Referance;
using System.Collections.Generic;

namespace HealthPlan.Utilities
{
	public class CustomResponse<T>
	{
		public bool Success { get;  set; } = true;
		public string Message { get;  set; }
		public T Response { get;  set; }
		public int Error_code { get;  set; } = 0;

		public static CustomResponse<T> SuccessMessage(string message, T data)
		{
			return new CustomResponse<T>
			{
				Response =  data,
				Message = message,
			};
		}

		public static CustomResponse<T> ErrorMessage(string message, int errorCode, T data = default(T))
		{
			return new CustomResponse<T>
			{
				Response = data,
				Message = message,
				Error_code = errorCode,
				Success = false
			};
		}
	}


	public class DataType<T>
	{
		 public T Data { get; set; }
		 public dynamic Urls { get; set; }
		 public List<SchemeMaster> schemesMaster { get; set; }
	}

	




}

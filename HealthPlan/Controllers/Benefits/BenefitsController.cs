﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthPlan.BusinessLogic.BenefitsBusinessManager;
using HealthPlan.Data.Models.Benefits;
using HealthPlan.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HealthPlan.Controllers.Benefits
{
	[Authorize(AuthenticationSchemes = "Bearer")]
	[Route("api/[controller]")]
	[ApiController]
	public class BenefitsController : ControllerBase
	{
		[Route("GetBenefits")]
		[HttpPost]
		public IActionResult GetBenefits(BenefitsRequest benefitsRequest)
		{

			try
			{
				var benefits = BenefitesBusinessManager.GetBenefits(benefitsRequest);
				if (benefits.Categories == null)
				{
					return Ok(CustomResponse<string>.ErrorMessage("No Data found", 404));
				}
				else
				{
					return Ok(CustomResponse<BenefitsResponse>.SuccessMessage("success", benefits));
				}

			}
			catch (Exception e)
			{
				return Ok(CustomResponse<string>.ErrorMessage(e.Message, 505));
			}
		}

		[Route("BenefitsResetCache")]
		[HttpGet]
		public IActionResult ResetCache()
		{
			BenefitesBusinessManager.ResetCache();
			return Ok();
		}
	}
}

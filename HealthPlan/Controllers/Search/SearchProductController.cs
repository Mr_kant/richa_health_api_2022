﻿
using HealthPlan.BusinessLogic.SearchBusinessManager;
using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Common.Global;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.Product;
using HealthPlan.Data.Models.SearchProduct;
using HealthPlan.Services;
using HealthPlan.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using SchemeMaster = HealthPlan.Data.Models.Referance.SchemeMaster;
using HealthPlan.Data.Models.GroupCare;

namespace HealthPlan.Controllers.SearchController
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class SearchProductController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IUriService uriService;
        List<ApiRequestResponseLog> ApiRequestResponseLog;

        public SearchProductController(IUriService uriService, IConfiguration config)
        {
            this.uriService = uriService;
            _config = config;
            AppSettings.IsProduction = _config["IsProduction"] == "false" ? false : true;
            AppSettings.LogPath = _config["LogPath"];
            ApiRequestResponseLog = new List<ApiRequestResponseLog>();

        }

        [HttpGet]
        [Route("ClearCache")]
        public IActionResult Clear()
        {
            var isSuccessFull = ProductDetailBAL.ClearCache();
            logger.Log($"Cache cleared {isSuccessFull} ", nameof(SearchProductController), 53);
            return Ok(CustomResponse<string>.SuccessMessage("success", "Cache cleared"));
        }


        [Route("searchProduct")]
        [HttpPost]
        public async Task<IActionResult> SearchProduct(Productfilter filterdata)
        {
            //code for age validation
            int mainage = 0;
            int errorCode = 400;
            if (!(filterdata.SelfAge == 0))
            {
                mainage = (int)filterdata.SelfAge;
            }
            else if (filterdata.SelfAge == 0 && !(filterdata.SpouseAge == 0))
            {
                mainage = (int)filterdata.SpouseAge;
            }
            else if (filterdata.SelfAge == 0 && filterdata.SpouseAge == 0 && !(filterdata.FatherAge == 0) && !(filterdata.MotherAge == 0))
            {
                mainage = (int)filterdata.FatherAge;
            }
            else if (filterdata.SelfAge == 0 && filterdata.SpouseAge == 0 && filterdata.FatherAge == 0 && !(filterdata.MotherAge == 0))
            {
                mainage = (int)filterdata.MotherAge;
            }

          
            if (mainage == 0)
            {
                return Ok(CustomResponse<string>.ErrorMessage("Insured Age is Required", errorCode));
            }

            if (filterdata.Suminsured == null || filterdata.Suminsured == "")
            {
                return Ok(CustomResponse<string>.ErrorMessage("Suminsured is required in this format exm. 200000-500000 ", errorCode));
            }
            if (!filterdata.Suminsured.ContainsInvariant("-"))
            {
                return Ok(CustomResponse<string>.ErrorMessage("suminsured is not in correct format send data in  this format e.g. 200000-500000 ", errorCode));
            }
            if (filterdata.policytype == null || filterdata.policytype == "")
            {
                return Ok(CustomResponse<string>.ErrorMessage("Policytype is required", errorCode));
            }
            if (filterdata.pincode == 0)
            {
                return Ok(CustomResponse<string>.ErrorMessage("Pincode is required", errorCode));
            }
            logger.Log("Validation OK", nameof(SearchProductController), 110);

            //schems genreate code 

            string schems;
            int Adults = 0;
            int child = 0;
            List<KeyValuePair<string, int?>> members = new List<KeyValuePair<string, int?>>();

            if (filterdata.SelfAge != null && filterdata.SelfAge != 0)
            {
                Adults = 1;
                members.Add(new KeyValuePair<string, int?>("SELF", filterdata.SelfAge));
            }
            if (filterdata.SpouseAge != null && filterdata.SpouseAge != 0)
            {
                Adults = Adults + 1;
                members.Add(new KeyValuePair<string, int?>("WIFE", filterdata.SpouseAge));
            }
            if (filterdata.FatherAge != null && filterdata.FatherAge != 0)
            {
                Adults = Adults + 1;
                members.Add(new KeyValuePair<string, int?>("FATH", filterdata.FatherAge));
            }
            if (filterdata.MotherAge != null && filterdata.MotherAge != 0)
            {
                Adults = Adults + 1;
                members.Add(new KeyValuePair<string, int?>("MOTH", filterdata.MotherAge));
            }
            if (filterdata.SonAge[0] != 0)
            {
                child = filterdata.SonAge.Length;
                foreach (var son in filterdata.SonAge)
                {
                    members.Add(new KeyValuePair<string, int?>("SON", son));
                }

            }
            if (filterdata.DaughterAge[0] != 0)
            {
                child = child + filterdata.DaughterAge.Length;
                foreach (var DaughterAge in filterdata.DaughterAge)
                {
                    members.Add(new KeyValuePair<string, int?>("UDTR", DaughterAge));
                }
            }

            if (child == 0 && Adults == 1)
            {
                schems = "A";
            }
            else if (child == 0)
            {
                schems = Adults.ToString() + "A";
            }

            else
            {
                schems = Adults.ToString() + "A" + "+" + child.ToString() + "C";
            }

            //end
            var searchList = new List<GroupCareQuotesResponse>();

            ProductSearchModel clientData = new ProductSearchModel();

            clientData = ProductDetailBAL.FilterProduct(filterdata.Suminsured, filterdata.policytype, mainage, schems);

            // get the all schems

            List<SchemeMaster> schemeMasters = ProductDetailBAL.Getschemes();
            var GroupcarepostResponses = SearchProductIndivisual.GetQuotesCareHealth(mainage, filterdata, members, clientData, ApiRequestResponseLog);
            searchList.AddRange(GroupcarepostResponses);
            logger.Log("Got Data list", nameof(SearchProductController), 201);
            logger.Log("Got Api List Data", nameof(SearchProductController), 207);
            var response = new DataType<List<GroupCareQuotesResponse>>();
            response.Data = searchList;
            response.Urls = null;
            response.schemesMaster = schemeMasters;
            ProductDetailBAL.ApiLog(ApiRequestResponseLog);
            logger.Log();
            return Ok(CustomResponse<DataType<List<GroupCareQuotesResponse>>>.SuccessMessage("success", response));
        }
    }
}
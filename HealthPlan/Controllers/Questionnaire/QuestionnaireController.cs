﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthPlan.BusinessLogic.ProposalBusinessManager;
using HealthPlan.Data.Models;
using HealthPlan.Data.Models.GeneralProposal;
using HealthPlan.Data.Models.Religare;
using HealthPlan.BusinessLogic.ReligareBusinessManager;
using HealthPlan.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace HealthPlan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class QuestionnaireController : ControllerBase
    {

        [Route("Questionnaire/{companyId}/{productId}")]
        [HttpGet]
        public IActionResult GetAllQuestions(int companyId, int? productId)
        {
            var questionList = new List<GeneralQuestion>();
            var subQuestionList = new List<GeneralQuestion>();

            if (companyId == 1 && productId > 0)
            {


                var QuestionList = ReligareCommonManager.GetQuestion(productId);

                var parentQuestions = QuestionList.Where(e => e.ParentId == 0).ToList();

                foreach (var item in parentQuestions)
                {
                    subQuestionList = new List<GeneralQuestion>();
                    var subQuestion = QuestionList.Where(e => e.ParentId == item.QuestionId).ToList();
                    foreach (var question in subQuestion)
                    {
                        GeneralQuestion subQuest = new GeneralQuestion()
                        {
                            QuestionDescription = question.QuestionDescription,
                            QuestionSetCode = question.QuestionSetCode,
                            QuestionCode = question.QuestionCode,
                            Response = question.Response,
                            SubQuestion = new List<GeneralQuestion>()
                        };

                        subQuestionList.Add(subQuest);
                    }
                    GeneralQuestion QList = new GeneralQuestion()
                    {
                        QuestionDescription = item.QuestionDescription,
                        QuestionSetCode = item.QuestionSetCode,
                        QuestionCode = item.QuestionCode,
                        Response = item.Response,
                        SubQuestion = subQuestionList
                    };
                    questionList.Add(QList);

                }


            }

            //if (companyId == 3)
            //{
            //    var QuestionList = ManipalCommonManager.GetQuestionManipal().ToList();

            //    foreach (var item in QuestionList)
            //    {
            //        GeneralQuestion QList = new GeneralQuestion()
            //        {
            //            QuestionCode = item.QuestionCode,
            //            QuestionSetCode = item.QuestionCode,
            //            QuestionDescription = item.Questionnaire,
            //            Response = item.Response
            //        };
            //        questionList.Add(QList);
            //    }


            //}
            return Ok(CustomResponse<List<GeneralQuestion>>.SuccessMessage("success", questionList));

        }
    }
}
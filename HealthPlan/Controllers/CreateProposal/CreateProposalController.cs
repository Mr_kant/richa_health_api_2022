﻿using HealthPlan.BusinessLogic.ProposalBusinessManager;
using HealthPlan.BusinessLogic.SearchBusinessManager;
using HealthPlan.Common.Global;
using HealthPlan.Data.Models.Authenticate;
using HealthPlan.Data.Models.Common;
using HealthPlan.Data.Models.GeneralProposal;
using HealthPlan.Services;
using HealthPlan.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthPlan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class CreateProposalController : ControllerBase
    {
        private IConfiguration _config;
        List<ApiRequestResponseLog> ApiRequestResponseLog;
        public CreateProposalController(IConfiguration config, IUriService uriService)
        {
            _config = config;
          
            AppSettings.IsProduction = _config["IsProduction"] == "false" ? false : true;
            AppSettings.LogPath = _config["LogPath"];
            AppSettings.ReligareAppId = _config["Religare:AppID"];
            AppSettings.ReligareSignature = _config["Religare:Signature"];
            AppSettings.ReligareTimeStamp = _config["Religare:TimeStamp"];
            AppSettings.BaseAgentId = _config["Religare:BaseAgentId"];
            AppSettings.DomianUrl = uriService.GetUrl();
            ApiRequestResponseLog = new List<ApiRequestResponseLog>();
        }
        [Route("CreateProposal")]
        [HttpPost]
        public async Task<IActionResult> CreateProposal(GeneralProposal proposal)
        {

            if (ModelState.IsValid)
            {
                ProposalManager pm = new ProposalManager();

                ResponseWithUrl<ProposalResponse> response = await pm.Proposal(proposal, ApiRequestResponseLog);

                ProductDetailBAL.ApiLog(ApiRequestResponseLog);
                if (response == null)
                {
                    return Ok(CustomResponse<string>.ErrorMessage("failed", 400, pm.Error));
                }

                return Ok(CustomResponse<ResponseWithUrl<ProposalResponse>>.SuccessMessage("success", response));
            }
            else
            {
                return Ok(CustomResponse<IEnumerable<ModelError>>.ErrorMessage("failed", 400, ModelState.Values.SelectMany(v => v.Errors)));
            }

        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using HealthPlan.Data.Models.Religare;
using HealthPlan.BusinessLogic.ReligareBusinessManager;
using HealthPlan.Utilities;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using HealthPlan.Data.Models.Referance;
using System.Security.Cryptography.Xml;
using static HealthPlan.Data.Models.Religare.ReligareMasterData;
using Microsoft.AspNetCore.Authorization;
using HealthPlan.Common.Global;
using HealthPlan.Services;
using System;
using HealthPlan.Common.ExtensionMethods;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthPlan.Controllers.Religare
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [Route("api/[controller]")]
    [ApiController]
    public class CareController : ControllerBase
    {
        private IConfiguration _config;

        public string AppID { get; }
        public string Signature { get; }
        public string TimeStamp { get; }
        public string AgentId { get; }

        public int CompanyId = 1;

        public CareController(IConfiguration config,IUriService uriService)
        {
            _config = config;
            AppID = _config["ReligarePolicyPDF:AppID"];
            Signature = _config["ReligarePolicyPDF:Signature"];
            TimeStamp = _config["ReligarePolicyPDF:TimeStamp"];
            AgentId = _config["ReligarePolicyPDF:agentId"];
            AppSettings.DomianUrl = uriService.GetUrl();
        }

        [HttpGet]
        [Route("GetPaymentUrl/{policyNumber}")]
        public IActionResult GetPaymentUrl(string policyNumber)
        {
            var url= $"{AppSettings.DomianUrl }/CarePayment/{policyNumber}";
            return Ok(CustomResponse<string>.SuccessMessage("Payment Url", url));
        }




        [AllowAnonymous]
        [HttpGet]
        [Route("PolicyDocument/{policyNumber}/{Inquiry_id}/{type}")]
        public async Task<IActionResult> PolicyPDF(string policyNumber, string Inquiry_id, string type = "POLSCHD")
        {

            string url = null;


            IEnumerable<Api> api = ReligareCommonManager.GetApiPolicyPDFDetails(CompanyId, "PolicyPDF");
            RestFullApi<ReligarePolicyPDF> rest = new RestFullApi<ReligarePolicyPDF>();
            rest.baseaddress = api.FirstOrDefault().BaseAddress;
            url = api.FirstOrDefault().Url;
            var requestheader = new List<KeyValuePair<string, string>>() {
                        new KeyValuePair<string, string>("AppID", AppID),
                        new KeyValuePair<string, string>("Signature", Signature),
                        new KeyValuePair<string, string>("TimeStamp", TimeStamp),
                        new KeyValuePair<string, string>("AgentId", AgentId)
                };
            ReligarePolicyPDF religarePolicyPDF = new ReligarePolicyPDF();
            religarePolicyPDF.intFaveoGetPolicyPDFIO = new IntFaveoGetPolicyPDFIO { policyNum = policyNumber, ltype = type };
            var value = await rest.DocumentPostApi(url, religarePolicyPDF, requestheader);

            if (rest.status == true)
            {
                var data = JsonConvert.DeserializeObject<ReligarePolicyPDFResponse>(value);

                if (data.responseData.status == "1")
                {
                    var stream = data.intFaveoGetPolicyPDFIO.dataPDF.ConvertBase64ToStream();
                    return new FileStreamResult(stream, "application/pdf");
                }
                else
                {

                    return Ok(CustomResponse<ReligarePolicyPDFResponse>.SuccessMessage("Polity document", data));
                }
            }
           
            return Ok(CustomResponse<object>.ErrorMessage("Not found", (int)rest.statusCode, value));

        }

       

        [HttpGet]
        [Route("ClearCache")]
        public IActionResult ClearCache()
        {
            var result = ReligareCommonManager.ClearCache();
            return Ok(CustomResponse<bool>.SuccessMessage("ClearCache", result));
        }




    }
    public class request
    {
        public string proposalNum { get; set; }
        public string returnURL { get; set; }
        public string name { get; set; }


}
}

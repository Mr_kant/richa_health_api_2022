﻿using HealthPlan.BusinessLogic.GroupCareBusinessManager;
using HealthPlan.Common.Global;
using HealthPlan.Data.Models.Referance;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace HealthPlan.Controllers.CarePaymentController
{
	public class CarePaymentController : Controller
	{
		[HttpGet]
		[Route("CarePayment/{proposalNumber}/{Inquiryid}")]
		public IActionResult Index(string proposalNumber, string Inquiryid)
		{
			IEnumerable<Api> api = GroupCareCommonManager.GetApiDetails(1, "Payment");
			IEnumerable<Api> PaymentSucess = GroupCareCommonManager.GetApiDetails(1, "PaymentSucess");
			ViewData["proposalNumber"] = proposalNumber;
			ViewData["Payment"] = $"{api.FirstOrDefault().BaseAddress}{api.FirstOrDefault().Url}"; 
			ViewData["CarePaymentSuccess"] = $"{PaymentSucess.FirstOrDefault().BaseAddress}{Inquiryid}";// 
			return View();
		}

		
	}
}

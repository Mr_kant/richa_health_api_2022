﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthPlan.Data.Models.Religare;
using Microsoft.AspNetCore.Mvc;

namespace HealthPlan.Controllers.CarePaymentController
{
	public class CarePaymentSuccessController : Controller
	{
		[HttpPost]
		[Route("CarePaymentSuccess")]
		public IActionResult Index(PaymentSuccess value)
		{

			return View(value);
		}

		[HttpGet]
		[Route("CarePaymentSuccess")]
		public IActionResult Index()
		{
			return View(new PaymentSuccess { errorFlag=  "",policyNumber= "0", transactionRefNum="0", uwDecision = ""  });
		}
	}
}

﻿using HealthPlan.BusinessLogic.AuthenticateBusinessManager;
using HealthPlan.Data.Models;
using HealthPlan.Data.Models.Authenticate;
using HealthPlan.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace HealthPlan.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private IConfiguration _config;
        public AuthenticateController( IConfiguration config)
        {
            _config = config;
        }
        
        [HttpPost]
         [Route("Login")]
        public IActionResult Login(LoginModel model)
        {
            //  IActionResult response = Unauthorized();

            ResponseModel res = AuthenticateBAL.Authenticate_User(model);
            Token token = new Token();
            if (res.code == 0 && res.message == "Success")
            {
                token.tokenvalue = GenerateJSONWebToken(model);
                return Ok(CustomResponse<Token>.SuccessMessage("mesasge", token));
            }
            return Ok(CustomResponse<ResponseModel>.SuccessMessage("mesasge", res));
        }


        private string GenerateJSONWebToken(LoginModel user)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName,user.Username),
            };
            try
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
                var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                  _config["Jwt:Issuer"],
                  null,
                  expires: DateTime.Now.AddMinutes(120),
                  signingCredentials: credentials);

                return new JwtSecurityTokenHandler().WriteToken(token);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        [HttpPost]
        [Route("RegisterUser")]
        public IActionResult RegisterUser(AddUserModel model)
        {
            ResponseModel response = AuthenticateBAL.AddUser(model);
            if (response.code == 0)
            {
                return Ok(CustomResponse<int>.SuccessMessage(response.message, response.code));
            }
            return Ok(CustomResponse<int>.ErrorMessage(response.message, response.code));
        }

    }
}
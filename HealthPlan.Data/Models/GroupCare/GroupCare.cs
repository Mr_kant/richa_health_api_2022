﻿using HealthPlan.Common.ExtensionMethods;
using HealthPlan.Common.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static HealthPlan.Data.Models.GroupCare.GroupCare;

namespace HealthPlan.Data.Models.GroupCare
{
    public class GroupCare
    {
        public class PartyAddressDOList
        {
            public string addressLine1Lang1 { get; set; }
            public string addressLine2Lang1 { get; set; }
            public string addressTypeCd { get; set; }
            public string areaCd { get; set; }
            public string cityCd { get; set; }
            public string pinCode { get; set; }
            public string stateCd { get; set; }
            public string countryCd { get; set; }
        }

        public class PartyContactDOList
        {
            public string contactTypeCd { get; set; }
            public long contactNum { get; set; }
            public string stdCode { get; set; }
        }

        public class PartyEmailDOList
        {
            public string emailAddress { get; set; }
            public string emailTypeCd { get; set; }
        }

        public class PartyIdentityDOList
        {
            public string identityTypeCd { get; set; }
            public string identityNum { get; set; }

        }

        public class PartyEmploymentDOList
        {
        }

        public class PartyDOList
        {
            public string guid { get; set; }
            
            public string firstName { get; set; }
          
           
            public string lastName { get; set; }
            public string roleCd { get; set; }
            public string birthDt { get; set; }
            public string genderCd { get; set; }
            public int height { get; set; }
            public int weight { get; set; }
            public string titleCd { get; set; }
            public string relationCd { get; set; }
            public List<object> partyQuestionDOList { get; set; }
            public List<PartyIdentityDOList> partyIdentityDOList { get; set; }
            public List<PartyAddressDOList> partyAddressDOList { get; set; }
            public List<PartyContactDOList> partyContactDOList { get; set; }
            public List<PartyEmailDOList> partyEmailDOList { get; set; }
           

           public List<PartyEmploymentDOList> partyEmploymentDOList { get; set; }
        }

        public class PolicyAdditionalFieldsDOList
        {
            public string field1 { get; set; }
            public string field10 { get; set; }
            public string field12 { get; set; }
            public string fieldAgree { get; set; }
            public string fieldAlerts { get; set; }
            public string fieldTc { get; set; }
        }

        public class Policy
        {
            public string businessTypeCd { get; set; }
            public string baseProductId { get; set; }
            public string baseAgentId { get; set; }
            public string coverType { get; set; }
            public string sumInsured { get; set; }
            public string addOns { get; set; }
            public int term { get; set; }
            public string isPremiumCalculation { get; set; }
            public List<PartyDOList> partyDOList { get; set; }
            public List<PolicyAdditionalFieldsDOList> policyAdditionalFieldsDOList { get; set; }
        
        }

        public class IntPolicyDataIO
        {
            public Policy policy { get; set; }
            public IntPolicyDataIO()
            {

            }

            // Constructor for Religare  Individual 
            public IntPolicyDataIO(GeneralProposal.GeneralProposal proposalInd, IEnumerable<GroupCareSuminsured> SuminsuredDetails)
            {
                // IntPolicyDataIO intPolicyDataIO = new IntPolicyDataIO();
                var policy = new Policy();
                policy.partyDOList = new List<PartyDOList>();

                var contactList = new List<PartyContactDOList>();
                foreach (var insured in proposalInd.Insureds)
                {
                    string role = "";
                    string guids = Guid.NewGuid().ToString();
                    if (policy.partyDOList.Count < 1)
                    {
                        role = "PROPOSER";
                    }

                //    role = "PRIMARY";

                    policy.partyDOList.Add(new PartyDOList
                    {
                        birthDt = insured.DOB.CareDate(),
                        firstName = insured.FirstName,
                        lastName = insured.LastName,
                        genderCd = insured.Gender.ToUpper(),
                        height = insured.Height,
                        weight = insured.Weight,
                        guid = guids,

                        partyAddressDOList = new List<PartyAddressDOList>
                    {
                        new PartyAddressDOList()
                        {
                            addressLine1Lang1=proposalInd.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalInd.ProposerDetails.AddLine2,
                            addressTypeCd="PERMANENT",
                            areaCd=proposalInd.ProposerDetails.City,
                            cityCd=proposalInd.ProposerDetails.City,
                            pinCode=proposalInd.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalInd.ProposerDetails.State,
                            countryCd="IND"
                        },
                        new PartyAddressDOList
                        {
                            addressLine1Lang1=proposalInd.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalInd.ProposerDetails.AddLine2,
                            addressTypeCd="COMMUNICATION",
                            areaCd=proposalInd.ProposerDetails.City,
                            cityCd=proposalInd.ProposerDetails.City,
                            pinCode=proposalInd.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalInd.ProposerDetails.State,
                            countryCd="IND"
                        }
                    },
                        partyContactDOList = new List<PartyContactDOList>
                    {
                       new PartyContactDOList()
                       {
                           contactTypeCd="MOBILE",
                           contactNum=Convert.ToInt64( proposalInd.ProposerDetails.MobileNumber),
                           stdCode="+91",
                       }
                    },
                        partyEmailDOList = new List<PartyEmailDOList>
                    {
                        new PartyEmailDOList
                        {
                            emailAddress=proposalInd.ProposerDetails.Email,
                            emailTypeCd="PERSONAL",
                        }
                    },

                        partyIdentityDOList = new List<PartyIdentityDOList>
                    {
                        new PartyIdentityDOList
                        {
                           identityTypeCd="PAN",
                           identityNum = proposalInd.ProposerDetails.panNumber
                        }
                    },
                        partyQuestionDOList = new List<object>()
                        { },

                        relationCd = "SELF",
                        roleCd = role,
                        titleCd = "MR",
                    });

                    policy.partyDOList.Add(new PartyDOList
                    {
                        birthDt = insured.DOB.CareDate(),
                        firstName = insured.FirstName,
                        lastName = insured.LastName,
                        genderCd = insured.Gender.ToUpper(),
                        height = insured.Height,
                        weight = insured.Weight,
                        guid = guids,


                        partyAddressDOList = new List<PartyAddressDOList>
                    {
                        new PartyAddressDOList()
                        {
                            addressLine1Lang1=proposalInd.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalInd.ProposerDetails.AddLine2,
                            addressTypeCd="PERMANENT",
                            areaCd=proposalInd.ProposerDetails.City,
                            cityCd=proposalInd.ProposerDetails.City,
                            pinCode=proposalInd.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalInd.ProposerDetails.State,
                            countryCd="IND"
                        },
                        new PartyAddressDOList
                        {
                            addressLine1Lang1=proposalInd.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalInd.ProposerDetails.AddLine2,
                            addressTypeCd="COMMUNICATION",
                            areaCd=proposalInd.ProposerDetails.City,
                            cityCd=proposalInd.ProposerDetails.City,
                            pinCode=proposalInd.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalInd.ProposerDetails.State,
                            countryCd="IND"
                        }
                    },
                        partyContactDOList = new List<PartyContactDOList>
                    {
                       new PartyContactDOList()
                       {
                           contactTypeCd="MOBILE",
                           contactNum=Convert.ToInt64( proposalInd.ProposerDetails.MobileNumber),
                           stdCode="+91",
                       }
                    },
                        partyEmailDOList = new List<PartyEmailDOList>
                    {
                        new PartyEmailDOList
                        {
                            emailAddress=proposalInd.ProposerDetails.Email,
                            emailTypeCd="PERSONAL",
                        }
                    },
                        partyEmploymentDOList = new List<PartyEmploymentDOList>
                        {
                            new PartyEmploymentDOList()
                            {
                            }

                        },
                        partyIdentityDOList = new List<PartyIdentityDOList>
                    {
                        new PartyIdentityDOList
                        {
                            identityTypeCd="PAN",
                           identityNum = proposalInd.ProposerDetails.panNumber
                        }
                    },
                        partyQuestionDOList = new List<object>()
                        {
                          
                        },

                        relationCd = "SELF",
                        roleCd = "PRIMARY",
                        titleCd = "MR",
                    });


                }
                var NomineName = proposalInd.NomineeDetails.NomineeFirstName + " " + proposalInd.NomineeDetails.NomineeLastName;

                policy.policyAdditionalFieldsDOList = new List<PolicyAdditionalFieldsDOList>
                {
                   new PolicyAdditionalFieldsDOList()
                   {
                       field1="Partner_5 Paisa-CB",
                       field10=NomineName,
                       field12=proposalInd.NomineeDetails.NomineeRelation,
                       fieldAgree="YES",
                       fieldAlerts="YES",
                       fieldTc="YES",
                   },
                };
                policy.businessTypeCd = "NEWBUSINESS";
                policy.baseAgentId = AppSettings.BaseAgentId;
                policy.coverType = "INDIVIDUAL";
                policy.baseProductId = SuminsuredDetails.FirstOrDefault().ProductId;  //"80001283";
                policy.sumInsured = SuminsuredDetails.FirstOrDefault().Plan_No;  //"001"
                if (!string.IsNullOrEmpty(proposalInd.addOns))
                {
                    policy.addOns = proposalInd.addOns;
                }                
                policy.term = 1;
                policy.isPremiumCalculation = "YES";
                this.policy = policy;
            }


            // Constructor for Religare  Floater

            public IntPolicyDataIO(GeneralProposal.GeneralProposal proposalFloat, Boolean indiv, IEnumerable<GroupCarePartyRelation> GCrelation, IEnumerable<GroupCareSuminsured> SuminsuredDetails)
            {
                var prosalTittle = "MR";
                string relationcode = "";
                var policy = new Policy();
                policy.partyDOList = new List<PartyDOList>();
                                     
                if (proposalFloat.ProposerDetails.Gender.ToLower() == "female")
                {
                    prosalTittle = "MS";
                }
                foreach (HealthPlan.Data.Models.GeneralProposal.Insured item in proposalFloat.Insureds)
                {
                    var title = "MR";
                    if (item.Gender.ToLower() == "female")
                    {
                        title = "MS";
                    }
                    relationcode = GCrelation.FirstOrDefault(x => x.RelationId == item.RelationshipId).RelationCode;
                    string role = "";
                    string guid = Guid.NewGuid().ToString();
                 //   string relationcode = "";
                   
                    if (policy.partyDOList.Count < 1)
                    {
                        //guid = Guid.NewGuid().ToString();
                        role = "PROPOSER";
                        DateTime ProposalDOB = Convert.ToDateTime(proposalFloat.ProposerDetails.proposerDob);
                        policy.partyDOList.Add(new PartyDOList
                        {
                            birthDt = ProposalDOB.CareDate(),
                            firstName = proposalFloat.ProposerDetails.FirstName,
                            genderCd = proposalFloat.ProposerDetails.Gender.ToUpper(),
                            height = item.Height,
                            weight = item.Weight,
                            guid = guid,
                            lastName = proposalFloat.ProposerDetails.LastName,
                            partyAddressDOList = new List<PartyAddressDOList>
                     {
                         new PartyAddressDOList
                         {
                             
                             addressLine1Lang1=proposalFloat.ProposerDetails.AddLine1,
                             addressLine2Lang1=proposalFloat.ProposerDetails.AddLine2,
                             addressTypeCd="PERMANENT",
                             areaCd=proposalFloat.ProposerDetails.State,
                             cityCd=proposalFloat.ProposerDetails.City,
                             pinCode=proposalFloat.ProposerDetails.PostalCode.ToString(),
                             stateCd=proposalFloat.ProposerDetails.State,
                             countryCd="IND",
                         },
                         new PartyAddressDOList()
                         {
                             addressLine1Lang1=proposalFloat.ProposerDetails.AddLine1,
                             addressLine2Lang1=proposalFloat.ProposerDetails.AddLine2,
                             addressTypeCd="COMMUNICATION",
                             areaCd=proposalFloat.ProposerDetails.State,
                             cityCd=proposalFloat.ProposerDetails.City,
                             stateCd=proposalFloat.ProposerDetails.State,
                             pinCode=proposalFloat.ProposerDetails.PostalCode.ToString(),
                             countryCd="IND",

                         }
                        },

                            partyContactDOList = new List<PartyContactDOList>
                    {
                        new PartyContactDOList
                        {
                           contactTypeCd="MOBILE",
                           contactNum= Convert.ToInt64(proposalFloat.ProposerDetails.MobileNumber),
                           stdCode="+91",
                        }
                    },
                            partyEmailDOList = new List<PartyEmailDOList>
                           {
                               new PartyEmailDOList
                               {
                                   emailAddress=proposalFloat.ProposerDetails.Email,
                                   emailTypeCd="PERSONAL",

                               }
                           },
                            partyIdentityDOList = new List<PartyIdentityDOList>
                           {
                               new PartyIdentityDOList
                               {
                                   identityTypeCd="PAN",
                                    identityNum=proposalFloat.ProposerDetails.panNumber

                               }
                           },
                            partyQuestionDOList = new List<object>
                            {

                            },
                            relationCd = "SELF",
                            roleCd = role,
                            titleCd = prosalTittle,
                        });
                    };

                    if (relationcode != "SELF")
                    {
                        
                        guid = Guid.NewGuid().ToString();
                    }
                    role = "PRIMARY";
                    policy.partyDOList.Add(new PartyDOList
                    {
                        birthDt = item.DOB.CareDate(),
                        firstName = item.FirstName,
                        lastName = item.LastName,
                        genderCd = item.Gender.ToUpper(),
                        height = item.Height,
                        weight = item.Weight,
                        guid = guid,


                        partyAddressDOList = new List<PartyAddressDOList>
                    {
                        new PartyAddressDOList()
                        {
                            addressLine1Lang1=proposalFloat.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalFloat.ProposerDetails.AddLine2,
                            addressTypeCd="PERMANENT",
                            areaCd=proposalFloat.ProposerDetails.City,
                            cityCd=proposalFloat.ProposerDetails.City,
                            pinCode=proposalFloat.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalFloat.ProposerDetails.State,
                            countryCd="IND",
                        },
                        new PartyAddressDOList
                        {
                            addressLine1Lang1=proposalFloat.ProposerDetails.AddLine1,
                            addressLine2Lang1=proposalFloat.ProposerDetails.AddLine2,
                            addressTypeCd="COMMUNICATION",
                            areaCd=proposalFloat.ProposerDetails.City,
                            cityCd=proposalFloat.ProposerDetails.City,
                            pinCode=proposalFloat.ProposerDetails.PostalCode.ToString(),
                            stateCd=proposalFloat.ProposerDetails.State,
                            countryCd="IND",
                        }
                    },
                        partyContactDOList = new List<PartyContactDOList>
                    {
                       new PartyContactDOList()
                       {
                           contactTypeCd="MOBILE",
                            contactNum=Convert.ToInt64( proposalFloat.ProposerDetails.MobileNumber),
                           stdCode="+91",
                       }
                    },
                        partyEmailDOList = new List<PartyEmailDOList>
                    {
                        new PartyEmailDOList
                        {
                            emailAddress=proposalFloat.ProposerDetails.Email,
                            emailTypeCd="PERSONAL",
                        }
                    },
                        partyEmploymentDOList = new List<PartyEmploymentDOList>
                        {
                            new PartyEmploymentDOList()
                            {
                            }

                        },
                        partyIdentityDOList = new List<PartyIdentityDOList>
                    {
                        new PartyIdentityDOList
                        {
                              identityTypeCd="PAN",
                              identityNum=proposalFloat.ProposerDetails.panNumber
                        }
                    },
                        partyQuestionDOList = new List<object>()
                        {
                        },
                        relationCd = relationcode,
                        roleCd = role,
                        titleCd = title,
                    });
                }
                var NomineName = proposalFloat.NomineeDetails.NomineeFirstName + " " + proposalFloat.NomineeDetails.NomineeLastName;

                policy.policyAdditionalFieldsDOList = new List<PolicyAdditionalFieldsDOList>
                {
                   new PolicyAdditionalFieldsDOList
                   {
                       field1="Partner_5 Paisa-CB",
                       field10=NomineName,
                       field12=proposalFloat.NomineeDetails.NomineeRelation,
                       fieldAgree="YES",
                       fieldAlerts="YES",
                       fieldTc="YES",
                   }
                };

                policy.businessTypeCd = "NEWBUSINESS";
                policy.baseAgentId = AppSettings.BaseAgentId;
                policy.coverType = "FAMILYFLOATER";
                policy.baseProductId = SuminsuredDetails.FirstOrDefault().ProductId;  //"80001283";
                policy.sumInsured = SuminsuredDetails.FirstOrDefault().Plan_No;  //"001";               
                if (!string.IsNullOrEmpty(proposalFloat.addOns))
                {
                    policy.addOns = proposalFloat.addOns;
                }
                policy.term = 1;
                policy.isPremiumCalculation = "YES";
                this.policy = policy;
            }

        }
    }

    public class GroupQuotationInd
    {
        public IntPolicyDataIO intPolicyDataIO { get; set; }
    }

}

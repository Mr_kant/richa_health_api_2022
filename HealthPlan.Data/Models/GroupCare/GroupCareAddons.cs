﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.GroupCare
{
    public class GroupCareAddons
    {
        public int GroupCareAddOnsId { get; set; }
        public string AddOns { get; set; }
        public string Value { get; set; }
        public int productDetailID { get; set; }
        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.GroupCare
{
    public class GroupCareSuminsured
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        public string Plan_No { get; set; }
        public string Coverage { get; set; }
        public string SumInsured { get; set; }
        public int CoverageId { get; set; }
    }
}

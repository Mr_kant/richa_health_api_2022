﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.GroupCare
{
    interface IGroupCareQuotesResponse
    {
        //      public int Premium { get; set; }

        public int ProductDetailID { get; set; }
        public string CompanyName { get; set; }
        public int CompanyId { get; set; }
        public int ProductCode { get; set; }
        public string ProductName { get; set; }
        public int GSTPercenatge { get; set; }
        public string period { get; set; }
        public int Tenure { get; set; }
        public string Plan { get; set; }
        public decimal discountPercent { get; set; }
        public string BasePremium { get; set; }
        public string Policytype { get; set; }
        public string premium { get; set; }
        public int SchemaId { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public string Inquiry_id { get; set; }
        public dynamic product { get; set; }
        public int SchemeId { get; set; }
        public string AgeBracket { get; set; }
        public string Scheme { get; set; }
        public int SumInsuredValue { get; set; }
        public int SuminsuredId { get; set; }
        public int SumInsuredCode { get; set; }
        public string logoUrl { get; set; }
    }
}

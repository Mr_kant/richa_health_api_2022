﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.GroupCare
{
    public interface IGroupCarePartyRelation
    {
        public int RelationId { get; set; }
        public string RelationCode { get; set; }
        public string RelationDescription { get; set; }
    }
}


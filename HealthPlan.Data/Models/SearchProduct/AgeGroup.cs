﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.SearchProduct
{
    public class Productfilter
    {
        public string Inquiry_id { get; set; }        
        public int? SelfAge { get; set; }
        public int? SpouseAge { get; set; }
        //public int[]  sonDaughterAge  { get; set; }
        public int?[] SonAge { get; set; }
        public int?[] DaughterAge { get; set; }
        //public int[] fatherMotherAge{ get; set; }
        public int? FatherAge { get; set; }
        public int? MotherAge { get; set; }
        public string Suminsured { get; set; }
        public string policytype { get; set; }
        public int pincode { get; set; }
    }
}

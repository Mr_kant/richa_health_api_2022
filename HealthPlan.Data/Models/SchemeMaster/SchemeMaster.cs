﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Referance
{
    public class SchemeMaster : ISchemeMaster
    {
        public int SchemeId { get; set; }
        public string Schemes { get; set; }
    }
}

﻿namespace HealthPlan.Data.Models.Referance
{
	public interface ISchemeMaster
	{
		int SchemeId { get; set; }
		string Schemes { get; set; }
	}
}
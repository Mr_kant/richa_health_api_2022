﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Authenticate
{
    public class LoginModel : ILoginModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        //public bool RememberMe { get; set; }
    }
}

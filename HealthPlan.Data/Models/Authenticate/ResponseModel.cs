﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Authenticate
{
    public class ResponseModel
    {
        public int code { get; set; }
        public string  message { get; set; }
    }

    public class ResponseWithUrl<T>
    {
        public T Data { get; set; }
        public dynamic Urls { get; set; }

        public string message { get; set; }
    }
}

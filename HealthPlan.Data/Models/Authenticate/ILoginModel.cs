﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Authenticate
{
    interface ILoginModel
    {
       string Username { get; set; }
       string Password { get; set; }
       //bool RememberMe { get; set; }
    }
}

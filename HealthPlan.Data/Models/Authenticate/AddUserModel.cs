﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Authenticate
{
   public class AddUserModel: IAddUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        //public bool IsApproved { get; set; }
        //public bool IsLockOut { get; set; }
        //public DateTime Createdate { get; set; }
        //public DateTime LastLoginDate { get; set; }
        //public int FailedPasswordAttemptCount { get; set; }
        public string Pan_No { get; set; }
        public string Address { get; set; }
        public string EmailId { get; set; }
        
    }
}

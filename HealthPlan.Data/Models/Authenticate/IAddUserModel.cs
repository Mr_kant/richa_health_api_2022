﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Authenticate
{
    interface IAddUserModel
    {
        string UserName { get; set; }
         string Password { get; set; }
         //bool IsApproved { get; set; }
         //bool IsLockOut { get; set; }
         //DateTime Createdate { get; set; }
         //DateTime LastLoginDate { get; set; }
         //int FailedPasswordAttemptCount { get; set; }
         string Pan_No { get; set; }
         string Address { get; set; }
         string EmailId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Religare
{
	public class PaymentSuccess
	{
		public string csrf { get; set; }
        public string policyNumber { get; set; }
		public string transactionRefNum { get; set; }
		public string uwDecision { get; set; }
		public string errorFlag { get; set; }
		public string errorMsg { get; set; }
	}
}

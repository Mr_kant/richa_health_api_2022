﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthPlan.Data.Models.Religare
{
    public class ReligarePolicyPDF
    {
        [Required]
        public IntFaveoGetPolicyPDFIO intFaveoGetPolicyPDFIO { get; set; }
    }
    
    

    public class ResponseData
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class IntFaveoGetPolicyPDFIO
    {
        public string policyNum { get; set; }
        public string dataPDF { get; set; }
        public string ltype { get; set; }
        public string policyPDFStatus { get; set; }
        public string parentAgentId { get; set; }
        public List<object> listErrorListList { get; set; }
        public List<object> errorLists { get; set; }
    }

    public class ReligarePolicyPDFResponse
    {
        public ResponseData responseData { get; set; }
        public IntFaveoGetPolicyPDFIO intFaveoGetPolicyPDFIO { get; set; }
    }


    public class PolicyPDFErrorResponse
    {
        public IntPolicyPDFDataIOResponcse intFaveoGetPolicyPDFIO { get; set; }

        public PolicyPDFResponse responseData { get; set; }
    }

    public class IntPolicyPDFDataIOResponcse : IntFaveoGetPolicyPDFIO
    {
        public PolicyPDFlistErrorList[] listErrorListList { get; set; }

        public PolicyPDFerrorLists[] errorLists { get; set; }
    }

    public class PolicyPDFResponse
    {
        public string status { get; set; }
        public string message { get; set; }
    }

    public class PolicyPDFerrorLists
    {
        public string guid { get; set; }
        public string status { get; set; }
        public string errDescription { get; set; }

    }

    public class PolicyPDFlistErrorList
    {
        public string guid { get; set; }
        public string status { get; set; }
        public string errDescription { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Religare
{
    public interface IQuestion
    {
        public int QuestionId { get; set; }
        public int ParentId { get; set; }
        public string QuestionSetCode { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionDescription { get; set; }
        public string Type { get; set; }
        public string Response { get; set; }
    }
}

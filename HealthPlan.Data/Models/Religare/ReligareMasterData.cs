using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthPlan.Data.Models.Religare
{
    public class ReligareMasterData
    {
        [Required]
        public IntPolicyDataIO intPolicyDataIO { get; set; }

        public class PartyQuestionDOList
        {
            [Required]
            public string questionSetCd { get; set; }
            [Required]
            public string questionCd { get; set; }
            [Required]
            public string response { get; set; }
        }

        public class PartyIdentityDOList
        {
            [Required]
            public string identityTypeCd { get; set; }
        }

        public class PartyAddressDOList
        {
            [Required]
            public string addressTypeCd { get; set; }
            [Required]
            public string addressLine1Lang1 { get; set; }
            [Required]
            public string addressLine2Lang1 { get; set; }
            [Required]
            public string stateCd { get; set; }
            [Required]
            public string cityCd { get; set; }
            [Required]
            public string pinCode { get; set; }
            [Required]
            public string areaCd { get; set; }
            [Required]
            public string countryCd { get; set; }
        }

        public class PartyContactDOList
        {
            [Required]
            public string contactTypeCd { get; set; }
            [Required]
            public string contactNum { get; set; }
            [Required]
            public string stdCode { get; set; }
        }

        public class PartyEmailDOList
        {
            [Required]
            public string emailTypeCd { get; set; }
            [Required]
            public string emailAddress { get; set; }
        }

        public class PartyDOList
        {
            [Required]
            public string guid { get; set; }
            [Required]
            public string firstName { get; set; }
            [Required]
            public string lastName { get; set; }
            [Required]
            public string roleCd { get; set; }
            [Required]
            public string birthDt { get; set; }
            [Required]
            public string genderCd { get; set; }
            [Required]
            public string titleCd { get; set; }
            [Required]
            public string relationCd { get; set; }
            [Required]
            public List<PartyAddressDOList> partyAddressDOList { get; set; }
            [Required]
            public List<PartyContactDOList> partyContactDOList { get; set; }
            [Required]
            public List<PartyEmailDOList> partyEmailDOList { get; set; }
            [Required]
            public List<PartyIdentityDOList> partyIdentityDOList { get; set; }
            [Required]
            public List<PartyQuestionDOList> partyQuestionDOList { get; set; }



        }

        public class PolicyAdditionalFieldsDOList
        {
            [Required]
            public string fieldAgree { get; set; }
            [Required]
            public string fieldTc { get; set; }
            [Required]
            public string fieldAlerts { get; set; }
            //[Required]
            //public string field1 { get; set; }
        }

        public class Policy
        {

            [Required]
            public string isPremiumCalculation { get; set; }
            [Required]
            public string businessTypeCd { get; set; }
            [Required]
            public string baseAgentId { get; set; }
            [Required]
            public string baseProductId { get; set; }
            [Required]
            public string sumInsured { get; set; }
            [Required]
            public string term { get; set; }
            [Required]
            public string coverType { get; set; }
            public double premium { get; set; }
            public string proposalNum { get; set; }
            public int CompanyId { get; set; }
            [Required]
            public List<PolicyAdditionalFieldsDOList> policyAdditionalFieldsDOList { get; set; }
            [Required]
            public List<PartyDOList> partyDOList { get; set; }

        }

        public class ProposalReq
        {
            public IntPolicyDataIO intPolicyDataIO { get; set; }

        }


        public class IntPolicyDataIO
        {
            [Required]
            public Policy policy { get; set; }


            public IntPolicyDataIO()
            {
            }

        }

        public class PurposalErrorResponse
        {
            public IntPolicyDataIOResponcse intPolicyDataIO { get; set; }

            public PerposalResponse responseData { get; set; }
        }

        public class IntPolicyDataIOResponcse : IntPolicyDataIO
        {
            public PerposalerrorLists[] listErrorListList { get; set; }

            public PerposalerrorLists[] errorLists { get; set; }
        }

        public class PerposalResponse
        {
            public string status { get; set; }
            public string message { get; set; }
        }

        public class PerposalerrorLists
        {
            public string guid { get; set; }
            public string status { get; set; }
            public string errDescription { get; set; }

        }

        public class PerposallistErrorList
        {
            public string guid { get; set; }
            public string status { get; set; }
            public string errDescription { get; set; }
        }

    }
}

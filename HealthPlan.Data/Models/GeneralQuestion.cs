﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models
{
    public class GeneralQuestion
    {
        public string QuestionSetCode { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionDescription { get; set; }
        public string Response { get; set; }

        public List<GeneralQuestion> SubQuestion { get; set; }
    }
}

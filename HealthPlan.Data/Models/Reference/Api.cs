﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Referance
{
    public class Api : IApi
    {
        public string ProductName { get; set; }
        public int ProductDetailId { get; set; }
        public string Method { get; set; }
        public string Url { get; set; }
        public string UrlType { get; set; }
        public string BaseAddress { get; set; }
    }
}

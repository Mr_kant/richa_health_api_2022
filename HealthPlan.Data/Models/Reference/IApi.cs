﻿namespace HealthPlan.Data.Models.Referance
{
	public interface IApi
	{
		int ProductDetailId { get; set; }
		string Method { get; set; }
		string Url { get; set; }
		string UrlType { get; set; }
		string BaseAddress { get; set; }
	}
}
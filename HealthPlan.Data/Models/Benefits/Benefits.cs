﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Benefits
{
	public class BenefitsRequest
	{
		public int CompanyId { get; set; }
		public int Productid { get; set; }
		public string Plan { get; set; }
		public long SumInsured { get; set; }

	}

	public class BenefitsResponse : BenefitsRequest
	{
		public List<Category> Categories { get; set; }
		public string LogoUrl { get; set; }

		public string ProductName { get; set; }
		public string InsuranceCompany { get; set; }

	}

	public class Category
	{ 
		public string CategoryName { get; set; }

		public int CategoryID { get; set; }
		public List<Menu> Menus { get; set; }

	}

	public class Menu
	{
		public string MenuName { get; set; }
		public string Details { get; set; }
		public int MenuID { get; set; }
		public string Destriptions { get; set; }

	
	}

	public class BenefitsType 
	{ 
	    public List<TableMenu> tableMenu { get; set; }
		public List<Destription> destriptions { get; set; }

		public CompanyItems CompanyDetails { get; set; }

		public string ProductName { get; set; }

	}

	public class CompanyItems
	{
		public string LogoUrl { get; set; }
		public string CompanyName { get; set; }
	}

	public class TableMenu
	{
		public int MenuID { get; set; }
		public string Menu { get; set; }
		public int CategoryID { get; set; }

		public string Category { get; set; }
		public string Description { get; set; }

	}

	public class Destription: BenefitsRequest
	{
		public string CompanyName { get; set; }
		public string ProductName { get; set; }
		public string Hospital_Network { get; set; }
		public string No_Claim_Bonus { get; set; }
		public string Co_Payment { get; set; }
		public string Room_Rent_Eligibility { get; set; }
		public string In_Patient_Hospitalization { get; set; }
		public string Before_Hospitalization { get; set; }
		public string After_Hospitalization { get; set; }
		public string Day_Care_Treatments { get; set; }
		public string Free_Health_Checkups { get; set; }
		public string AYUSH_Treatment { get; set; }
		public string OPD_Care { get; set; }
		public string Maternity { get; set; }
		public string Restore_Benefits { get; set; }
		public string Initial_Waiting_Period { get; set; }
		public string Existing_Disease_Waiting { get; set; }
		public string Organ_Donor_Cover { get; set; }
		public string Emergency_Ambulance { get; set; }
		public string Air_Ambulance { get; set; }
		public string Worldwide_Emergency { get; set; }
		public string Hospitalization_at_Home { get; set; }
		public string Second_Opinion { get; set; }

	}

}

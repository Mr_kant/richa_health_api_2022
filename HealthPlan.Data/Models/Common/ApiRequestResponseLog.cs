﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HealthPlan.Data.Models.Common
{
	public class ApiRequestResponseLog
	{
		public string Inquiry_id { get; set; }
		public string CompanyName { get; set; }
		public string Request { get; set; }
		public string Response { get; set; }
		public string ApiMethod { get; set; }

	}
}

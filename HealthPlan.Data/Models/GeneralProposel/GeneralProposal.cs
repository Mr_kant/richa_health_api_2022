﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthPlan.Data.Models.GeneralProposal
{
    public class GeneralProposal
    {
       // [Required][Range(1, 2147483640)]
        //public int Id { get; set; }
        public int ProductDetailId { get; set; }
        public string quoteId { get; set; }
        public Proposer ProposerDetails { get; set; }
        [Required]
        public DateTime starton { get; set; }
        public DateTime endon { get; set; }    
       public Nominee NomineeDetails { get; set; }
        [Required]
        public List<Insured> Insureds { get; set; }
        public string discountPercent { get; set; }
        public string premium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
      //public string logoUrl { get; set; };
        public int schemaId { get; set; }
        //public string InsuranceCompany { get; set; }
        [Required]
        public int InsuranceCompanyId { get; set; }
        [Required]
        public string PolicyType { get; set; }
        public int SumInsured { get; set; }
        public int SumInsuredId { get; set; }
        public Product products { get; set; }
        public string addOns { get; set; }
        public dynamic productTerms { get; set; }
        [Required]
        public string Inquiry_id { get; set; }
    }


    public class Proposer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string MobileNumber { get; set; }
        public string Email { get; set; }
        public string AddLine1 { get; set; }
        public string AddLine2 { get; set; }
        public string Landmark { get; set; }
        [Required]
        public int PostalCode { get; set; }
        public string zoneCd { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string proposerDob { get; set; }
        public string panNumber { get; set; }
        //public string gstTypeId { get; set; }
        //public string gstIdNumber { get; set; }
        public string aadharNumber { get; set; }
        //public string eiaNumber { get; set; }
        //public string socialStatus { get; set; }
        //public string socialStatusBpl { get; set; }
        //public string socialStatusDisabled { get; set; }
        //public string socialStatusInformal { get; set; }
        //public string socialStatusUnorganized { get; set; }
        public string previousMedicalInsurance { get; set; }
        [Required]
        public string annualIncome { get; set; }
        public string criticalIllness { get; set; }
    }
    public class Nominee
    {
        public string NomineeFirstName { get; set; }
        public string NomineeLastName { get; set; }
        public string NomineeAge { get; set; }
        public string NomineeRelation { get; set; }
        // public string NomineePercentClaim { get; internal set; }
    }
    public class Insured
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public DateTime DOB { get; set; }
        [Required]
        public string Gender { get; set; }
        public string Illness { get; set; }
        //public int SumInsuredId { get; set; }
        public int RelationshipId { get; set; }
        public int OccupationId { get; set; }
        public string IspersonalAccidentApplicable { get; set; }
        public string EngageManualLabour { get; set; }
        public string EngageWinterSports { get; set; }
        public int Height { get; set; }
        //public int HeightInch { get; set; }
        public int Weight { get; set; }
        public int BuyBackPED { get; set; }

        public List<QuestionList> QuestionsList { get; set; }
        //public PartyDOList partyDOList { get; set; }
    }

    public class Product
    {
        public string PolicyTypeName { get; set; }
        public string PolicyName { get; set; }
        //public string PostalCode { get; set; }
        public string Period { get; set; }
       // public int SchemeId { get; set; }
        
    }

    public class QuestionList
    {
        public string QuestionSetCode { get; set; }
        public string QuestionCode { get; set; }
        public string QuestionDescription { get; set; }
        public string Response { get; set; }
    }

    public class ProposalResponse
    {
        public string referenceId { get; set; }
        public string premium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public string ProposalNum { get; set; }
        public object productDetailId { get; set; }
        public int CompanyId { get; set; }
        public string Inquiry_id { get; set; }
       // public int Id { get; set; }
    }
}
